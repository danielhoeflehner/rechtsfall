<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('/home', 'HomeController@index')->middleware('auth')->name('home.get');

Route::get('/admin/settings', 'SettingsController@index')->middleware('admin')->name('admin.settings.get');

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login.get');
Route::post('/login', 'Auth\LoginController@login')->name('login.post');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout.get');

Route::post('/register', 'Auth\RegisterController@register')->middleware('guest', 'throttle:5,30')->name('register.post');

Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->middleware('guest')->name('password.reset.get');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->middleware('guest')->name('password.reset.token.get');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->middleware('guest')->name('password.email.post');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->middleware('guest')->name('password.reset.post');

Route::get('/post', 'PostController@index')->middleware('auth')->name('post.get');
Route::get('/post/create', 'PostController@create')->middleware('auth')->name('post.create.get');
Route::post('/post/create', 'PostController@store')->middleware('auth')->name('post.create.post');
Route::get('/post/{post}/show', 'PostController@show')->middleware('auth')->name('post.show.get');
Route::get('/post/{post}/edit', 'PostController@edit')->middleware('auth')->name('post.edit.get');
Route::post('/post/{post}/update', 'PostController@update')->middleware('auth')->name('post.update.post');
Route::post('/post/{post}/solved', 'PostController@solved')->middleware('auth')->name('post.solved.post');
Route::post('/post/{post}/answer/store', 'PostAnswerController@store')->middleware('auth')->name('post.answer.store.post');
Route::post('/answer/{answer}/vote', 'PostAnswerController@vote')->middleware('auth')->name('answer.vote.post');

Route::get('/tag/{tag}', 'PostController@showByTag')->middleware('auth')->name('tag.posts.get');
Route::post('/tag/search', 'TagController@search')->middleware('auth')->name('tag.search.post');

Route::get('/user/id/{user}', 'UserController@profile')->middleware('auth')->name('user.get');
Route::get('/user/profile', 'UserController@profile')->middleware('auth')->name('user.profile.get');
Route::get('/user/profilePic', 'UserController@profilePic')->middleware('auth')->name('user.profilepic.get');
Route::get('/user/settings/{tab?}', 'UserController@settings')->middleware('auth')->name('user.settings.get');
Route::post('/user/settings/general', 'UserController@saveGeneral')->middleware('auth')->name('user.settings.general.post');
Route::post('/user/settings/profile', 'UserController@saveProfile')->middleware('auth')->name('user.settings.profile.post');
Route::post('/user/settings/password', 'UserController@savePassword')->middleware('auth')->name('user.settings.password.post');


/*Route::group(['prefix' => '/password', 'middleware' => 'guest'], function () {
    Route::get('/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::post('/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::get('/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
    Route::post('/reset', 'Auth\ResetPasswordController@reset');
});*/

/*Route::group(['prefix' => '/post', 'middleware' => 'auth'], function () {
    Route::get('/', 'PostController@index');

    Route::get('/create', 'PostController@create');
    Route::post('/store', 'PostController@store');

    Route::get('/{p_id}/show', 'PostController@show');
    Route::get('/{p_id}/edit', 'PostController@edit');
    Route::post('/{p_id}/update', 'PostController@update');
    Route::post('/{p_id}/confirm', 'PostController@confirm');

    Route::group(['prefix' => '/{p_id}/answer'], function () {
        Route::post('/store', 'PostAnswerController@store');
        Route::post('/{pa_id}/vote', 'PostAnswerController@vote');
    });

    Route::get('/tag/{t_id}', 'PostController@showByTag');
});

Route::group(['prefix' => '/tag', 'middleware' => 'auth'], function () {
    Route::post('/search', 'TagController@search');
});

Route::group(['prefix' => '/user', 'middleware' => 'auth'], function () {
    Route::get('/', 'UserController@profile');

    Route::get('/profile', 'UserController@profile');
    Route::get('/profilePic', 'UserController@profilePic')->name('profilePic');

    Route::get('/settings/{tab?}', 'UserController@settings');
    Route::post('/settings/general', 'UserController@saveGeneral');
    Route::post('/settings/profile', 'UserController@saveProfile');
    Route::post('/settings/password', 'UserController@savePassword');
});*/


