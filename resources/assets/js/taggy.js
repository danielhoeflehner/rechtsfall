'use strict';

(function(root) {

    root.Taggy = function (elem, options) {
        if (elem.charAt(0) == '#') {
            elem = elem.slice(1);
        }

        this.tags = document.getElementById(elem);

        if (this.tags === null) {
            throw new Error("No element defined.");
        }

        this.tag_input = this.tags.querySelector('#tag-input');
        this.tag_items = this.tags.querySelectorAll('.tag-item i');
        this.tag_sug = document.getElementById('tag-sug');

        this.xhr_tag_url = options.url || '/tag/search';
        this.xhr_tag_method = options.method || 'POST';

        this.key_up = 38;
        this.key_down = 40;
        this.key_esc = 27;
        this.key_enter = 13;
    }

    root.Taggy.prototype = {

        constructor: Taggy,

        init: function () {
            this.positionTagSug();
            this.initTagInput();
            this.initTagItems();
        },

        initTagInput: function () {
            var pos = -1;
            var prev_pos = 0;

            // Register the enter key
            this.tag_input.addEventListener('keydown', function (event) {
                var tag_count = this.tagCount();
                var code = event.keyCode ? event.keyCode : event.which;

                if (code == this.key_enter && tag_count <= 4) {
                    pos = 0;
                    prev_pos = 0;
                    this.addTag();

                    event.preventDefault();
                } else if (code == this.key_enter && tag_count > 4) {
                    pos = 0;
                    prev_pos = 0;
                    this.tag_input.value = '';

                    event.preventDefault();
                }
            }.bind(this));


            // Register all other keys
            this.tag_input.addEventListener('keyup', function (event) {
                event.preventDefault();
                event.stopPropagation();

                var tag_suggs = this.getTagSuggs();
                var tag_suggs_len = tag_suggs.length;
                var code = event.keyCode ? event.keyCode : event.which;

                if ((code == this.key_down || code == this.key_up) && tag_suggs_len > 0) {
                    if (code == this.key_down) {

                        prev_pos = pos;
                        pos++;

                        if (pos > (tag_suggs_len - 1)) {
                            pos = tag_suggs_len - 1;
                            return;
                        }

                        tag_suggs[pos].classList.add('selected');

                        if (pos > 0 && pos < tag_suggs_len) {
                            tag_suggs[prev_pos].classList.remove('selected');
                        }
                    } else if (code == this.key_up) {

                        prev_pos = pos;
                        pos--;

                        if (pos < 0) {
                            pos = 0;
                            return;
                        }

                        tag_suggs[pos].classList.add('selected');

                        if (pos >= 0 && pos < tag_suggs_len) {
                            tag_suggs[prev_pos].classList.remove('selected');
                        }
                    }
                } else if (code == this.key_esc) {
                    pos = -1;
                    prev_pos = 0;
                    this.tag_sug.style.display = 'none';
                    this.tag_sug.innerHTML = '';
                } else {
                    pos = -1;
                    prev_pos = 0;
                    var input = this.tag_input.value.trim();

                    if (input.length > 0) {
                        var data = { search: input };
                        XHR(this.xhr_tag_method, this.xhr_tag_url, 'json', data, this.generateTagSuggestions.bind(this));
                    } else {
                        this.tag_sug.style.display = 'none';
                    }
                }
            }.bind(this));
        },

        initTagItems: function () {
            if (this.tag_items) {
                this.tag_items.forEach(function (item) {
                    item.addEventListener('click', this.deleteTag.bind(this));
                }, this);
            }
        },

        generateTagSuggestions: function (data) {
            if (data.error == false) {
                this.tag_sug.innerHTML = '';

                var suggs = data.suggs;
                if (suggs.length > 0 && this.tagCount() < 5) {
                    suggs.forEach(function (sug) {
                        var sug_div = document.createElement('div');
                        var sug_value = document.createTextNode(sug.value);

                        sug_div.appendChild(sug_value);
                        sug_div.classList.add('tag-sug-item');
                        sug_div.dataset.id = sug.id;

                        sug_div.addEventListener('click', function (event) {
                            this.addTag(sug);
                        }.bind(this));

                        this.tag_sug.appendChild(sug_div);
                    }.bind(this));

                    this.tag_sug.style.display = 'block';
                } else {
                    this.tag_sug.style.display = 'none';
                }

            } else {
                this.tag_sug.style.display = 'none';
            }
        },

        getTagSuggs: function () {
            return this.tag_sug.querySelectorAll('.tag-sug-item');
        },

        tagCount: function () {
            return this.tags.querySelectorAll('.tag-item').length;
        },

        positionTagSug: function () {
            this.tag_sug.style.top = this.tag_input.offsetHeight + 3 + 'px';
            this.tag_sug.style.left = this.tag_input.offsetLeft + 'px';
        },

        addTag: function (sug_item) {
            var selected_tag = this.tag_sug.querySelector('.tag-sug-item.selected');
            if (selected_tag && sug_item == null) {
                sug_item = {};
                sug_item.id = selected_tag.dataset.id;
                sug_item.value = selected_tag.innerHTML;
            }

            if (this.tag_input.value.trim().length < 3 && sug_item == null) {
                return;
            }

            var parent = this.tag_input.parentNode;
            var newTag = document.createElement('span');
            var text = sug_item ? document.createTextNode(sug_item.value) : document.createTextNode(this.tag_input.value.trim());
            var i = document.createElement('i');
            var hidden_input = document.createElement('input');

            newTag.classList.add('tag-item');
            newTag.appendChild(text);

            i.classList.add('lnr', 'lnr-cross');
            i.title = 'Tag löschen';
            i.addEventListener('click', this.deleteTag.bind(this));

            newTag.appendChild(i);

            hidden_input.type = 'hidden';

            if (sug_item != null) {
                hidden_input.name = 'post_tags[][' + sug_item.id + ']';
                hidden_input.value = sug_item.value;
            } else {
                hidden_input.name = 'post_tags[][new]';
                hidden_input.value = this.tag_input.value.trim();
            }

            newTag.appendChild(hidden_input);

            parent.insertBefore(newTag, this.tag_input);

            this.positionTagSug();
            this.tag_input.value = '';
            this.tag_sug.innerHTML = '';
            this.tag_sug.style.display = 'none';

            return;
        },

        deleteTag: function (event) {
            var tag = event.target.parentNode;

            tag.removeEventListener('click', this.deleteTag);
            tag.remove();

            this.positionTagSug();
        }
    }
}(window));