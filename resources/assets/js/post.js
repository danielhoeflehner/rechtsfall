'use strict';

document.addEventListener('DOMContentLoaded', function (event) {

    var btnPostSolved = document.getElementById('post-set-solved');

    if (btnPostSolved) {
        btnPostSolved.addEventListener('click', setPostSolved);
    }

});

function setPostSolved(event) {
    event.stopPropagation();

    let btn = this;

    btn.removeEventListener('click', setPostSolved);
    btn.setAttribute('disabled', '');
    btn.innerHTML += "<i class='fa fa-refresh fa-spin margin-left-10'></i>";

    const pid = parseInt(btn.dataset.pid);

    const url = '/post/' + pid + '/solved';
    const data = { type: true };

    XHR('POST', url, 'json', data, function (result) {
        if (result.error === false) {
            btn.innerHTML = 'Fall ist gelöst. <i class="lnr lnr-checkmark-circle margin-left-10"></i>';
        }
    });
}


