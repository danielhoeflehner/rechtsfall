'use strict';

document.addEventListener('DOMContentLoaded', function (event) {

    let postAnswers = document.getElementById('post-answers');

    if (postAnswers) {
        let btnsUpvote = postAnswers.querySelectorAll('.up');
        let btnsDownvote = postAnswers.querySelectorAll('.down');

        if ((btnsUpvote && btnsUpvote.length > 0) && (btnsDownvote && btnsDownvote.length > 0)) {
            btnsDownvote.forEach(function (btn) {
                if (btn.hasAttribute('disabled')) return;

                btn.addEventListener('click', sendAnswerVoteDown);
            });

            btnsUpvote.forEach(function (btn) {
                if (btn.hasAttribute('disabled')) return;

                btn.addEventListener('click', sendAnswerVoteUp);
            });
        }
    }

});

function sendAnswerVoteDown(event)
{
    event.stopPropagation();

    let btnDown = this;
    const pid = parseInt(btnDown.dataset.pid);
    const paid = parseInt(btnDown.dataset.paid);
    let countDown = parseInt(btnDown.dataset.count);

    let btnUp = document.getElementById('btn-up-' + paid);
    const countUp = parseInt(btnUp.dataset.count);

    btnUp.setAttribute('disabled', '');
    btnDown.setAttribute('disabled', '');

    btnDown.removeEventListener('click', sendAnswerVoteDown);
    btnUp.removeEventListener('click', sendAnswerVoteUp);

    const url = '/answer/' + paid + '/vote';
    const data = { type: 'down' };

    XHR('POST', url, 'json', data, function (result) {
        if (result.error == false) {
            countDown++;
            btnDown.dataset.count = countDown;
            setCounter(countDown, countUp, paid);
        }
    });
}

function sendAnswerVoteUp(event)
{
    event.stopPropagation();

    let btnUp = this;
    const pid = parseInt(btnUp.dataset.pid);
    const paid = parseInt(btnUp.dataset.paid);
    let countUp = parseInt(btnUp.dataset.count);

    let btnDown = document.getElementById('btn-down-' + paid);
    const countDown = parseInt(btnDown.dataset.count);

    btnUp.setAttribute('disabled', '');
    btnDown.setAttribute('disabled', '');

    btnUp.removeEventListener('click', sendAnswerVoteUp);
    btnDown.removeEventListener('click', sendAnswerVoteDown);

    var url = '/answer/' + paid + '/vote';
    var data = { type: 'up' };

    XHR('POST', url, 'json', data, function (result) {
        if (result.error == false) {
            countUp++;
            btnUp.dataset.count = countUp;
            setCounter(countDown, countUp, paid);
        }
    });
}

function setCounter(countDown, countUp, paid)
{
    let counter = document.getElementById('counter-' + paid);

    counter.innerHTML = countUp - countDown;
}


