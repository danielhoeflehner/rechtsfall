@extends('layout')

@section('content')
    <div class="row margin-top-50">
        <div class="small-12 medium-2 large-3 columns">&nbsp;</div>
        <div class="small-12 medium-8 large-6 columns">
            <form id="login-form" class="form" method="post" action="{{ route('register.post') }}" autocomplete="off">
                <div>
                    <p class="form-header">Registrieren</p>
                </div>
                <div class="my-form-control">
                    <input type="text" name="firstName" value="{{ old('firstName') }}">
                    <div class="underline"></div>
                    <label for="first_name">Vorname</label>
                </div>
                <div class="my-form-control">
                    <input type="text" name="lastName"  value="{{ old('lastName') }}">
                    <div class="underline"></div>
                    <label for="last_name">Nachname</label>
                </div>
                <div class="my-form-control">
                    <input type="text" name="email"  value="{{ old('email') }}">
                    <div class="underline"></div>
                    <label for="email">Email</label>
                </div>
                <div class="my-form-control">
                    <input type="password" name="password" required>
                    <div class="underline"></div>
                    <label for="password">Passwort</label>
                </div>
                <div class="my-form-control">
                    <input type="password" name="password_confirmation">
                    <div class="underline"></div>
                    <label for="password_confirm">Passwort bestätigen</label>
                </div>
                {{ csrf_field() }}
                <button class="button btn-blue" type="submit" name="btn_register">Registrieren</button>
            </form>
        </div>
        <div class="small-12 medium-2 large-3 columns">&nbsp;</div>
    </div>
<!--<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->
@endsection
