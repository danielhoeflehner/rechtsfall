@extends('layout')

@section('scripts')
    <script src="/js/login.js"></script>
@endsection

@section('content')
    <div class="row justify-content-center" id="login-form">
        <div class="col-md-8 col-lg-3 tank tank-auth">
            <h1 class="tank-auth__logo">
                rechtsfall.at<br>
                <span>Gemeinsam Fälle lösen</span>
            </h1>
            <ul class="nav nav-tabs nav-justified tank-auth__tabs" role="tablist">
                <li class="nav-item">
                    <a href="#login" class="nav-link active" data-toggle="tab" role="tab">Login</a>
                </li>
                <li class="nav-item">
                    <a href="#register" class="nav-link" data-toggle="tab" role="tab">Registrieren</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="login" role="tabpanel">
                    <form class="form tank-auth__form" method="post" action="{{ route('login.post') }}" autocomplete="on">
                        <div class="row tank-auth__form-control">
                            <label class="col-2 col-md-3 align-self-center auth-label" for="login-email">
                                <span class="hidden-sm-down">Email</span> <span class="pull-right text-right"><span title="Email Adresse" class="lnr lnr-envelope"></span></span>
                            </label>
                            <div class="col-10 col-md-9">
                                <input class="auth-input" type="email" name="email" id="login-email" placeholder="deine@email.com" required>
                            </div>
                        </div>
                        <div class="row tank-auth__form-control">
                            <label class="col-2 col-md-3 align-self-center" for="login-password">
                                <span class="hidden-sm-down">Passwort</span> <span class="pull-right text-right"><span title="Passwort" class="lnr lnr-lock"></span></span>
                            </label>
                            <div class="col-10 col-md-9">
                                <input class="auth-input" type="password" name="password" id="login-password" placeholder="Passwort" required>
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="text-center">
                            <button class="btn btn-primary btn-main" type="submit">Einloggen</button>
                        </div>
                    </form>
                    <div class="margin-bottom-20 text-center">
                        <a href="{{ route('password.reset.get') }}">Passwort vergessen</a>
                    </div>
                </div>
                <div class="tab-pane" id="register" role="tabpanel">
                    <form class="form tank-auth__form" method="post" action="{{ route('register.post') }}" autocomplete="off">
                        <div class="row tank-auth__form-control">
                            <label class="col-2 col-md-3 align-self-center" for="register-firstname">
                                <span class="hidden-sm-down">Vorname</span> <span class="pull-right text-right"><span title="Vorname" class="lnr lnr-user"></span></span>
                            </label>
                            <div class="col-10 col-md-9">
                                <input class="auth-input" type="text" name="firstName" id="register-firstname" value="{{ old('firstName') }}" placeholder="Max" required>
                            </div>
                        </div>
                        <div class="row tank-auth__form-control">
                            <label class="col-2 col-md-3 align-self-center" for="register-lastname">
                                <span class="hidden-sm-down">Nachname</span> <span class="pull-right text-right"><span title="Nachname" class="lnr lnr-users"></span></span>
                            </label>
                            <div class="col-10 col-md-9">
                                <input class="auth-input" type="text" name="lastName" id="register-lastname" value="{{ old('lastName') }}" placeholder="Mustermann" required>
                            </div>
                        </div>
                        <div class="row tank-auth__form-control">
                            <label class="col-2 col-md-3 align-self-center auth-label" for="register-email">
                                <span class="hidden-sm-down">Email</span> <span class="pull-right text-right"><span title="Email Adresse" class="lnr lnr-envelope"></span></span>
                            </label>
                            <div class="col-10 col-md-9">
                                <input class="auth-input" type="email" name="email" id="register-email" value="{{ old('email') }}" placeholder="max@mustermann.at" required>
                            </div>
                        </div>
                        <div class="row tank-auth__form-control">
                            <label class="col-2 col-md-3 align-self-center" for="register-password">
                                <span class="hidden-sm-down">Passwort</span> <span class="pull-right text-right"><span title="Passwort" class="lnr lnr-lock"></span></span>
                            </label>
                            <div class="col-10 col-md-9">
                                <input class="auth-input" type="password" name="password" id="register-password" placeholder="Passwort" required>
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="text-center">
                            <button class="btn btn-primary btn-main" type="submit">Registrieren</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center" id="login-info">
        <div class="col-md-8 col-lg-3 text-center">
            <div class="login-info-body">
                <p class="text-left">
                    rechtsfall.at bietet dir die Möglichkeit, gemeinsam mit anderen an Rechtsfällen zu arbeiten und zu versuchen, diese zu lösen.
                    Du kannst selber einen Fall erstellen oder bei schon bestehenden Fällen Lösungen dazuschreiben. Antworten bzw. Lösungen auf Fälle können
                    per Up- und Down-Vote bewertet werden. Positiv bewertete Anworten werden in der Liste oben eingereiht - negative Antworten wandern nach unten.
                    Du kannst auch Stars sammeln - je mehr deiner Antworten positiv sind, desto höher deine Stars.
                    <br><br>
                    <a href="/imprint" target="_blank">Impressum</a>
                </p>
            </div>
            <div class="text-center login-info-btn">
                <span class="lnr lnr-chevron-down"></span>
            </div>
        </div>

    </div>
@endsection