@extends('layout')

@section('scripts')
    <script src="/js/login.js"></script>
@endsection

@section('content')
    <div class="row justify-content-center" id="login-form">
        <div class="col-md-8 col-lg-3 tank tank-auth">
            <h1 class="tank-auth__logo">
                <span>Passwort zurücksetzen</span>
            </h1>
            <form class="form tank-auth__form" method="post" action="{{ route('password.reset.post') }}" autocomplete="on">
                <div class="row tank-auth__form-control">
                    <label class="col-2 col-md-3 align-self-center auth-label" for="login-email">
                        <span class="hidden-sm-down">Email</span> <span class="pull-right text-right"><span title="Email Adresse" class="lnr lnr-envelope"></span></span>
                    </label>
                    <div class="col-10 col-md-9">
                        <input class="auth-input" type="email" name="email" id="email" placeholder="deine@email.com" required>
                    </div>
                </div>
                <div class="row tank-auth__form-control">
                    <label class="col-2 col-md-3 align-self-center auth-label" for="password">
                        <span class="hidden-sm-down">Passwort</span> <span class="pull-right text-right"><span title="Passwort" class="lnr lnr-lock"></span></span>
                    </label>
                    <div class="col-10 col-md-9">
                        <input class="auth-input" type="password" name="password" id="password" placeholder="Passwort" required>
                    </div>
                </div>
                <div class="row tank-auth__form-control">
                    <label class="col-2 col-md-3 align-self-center auth-label" for="password-confirm">
                        <span class="hidden-sm-down">Passwort bestätigen</span> <span class="pull-right text-right"><span title="Passwort bestätigen" class="lnr lnr-lock"></span></span>
                    </label>
                    <div class="col-10 col-md-9">
                        <input class="auth-input" type="password" name="password_confirmation" id="password-confirm" placeholder="Passwort bestätigen" required>
                    </div>
                </div>
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="text-center">
                    <button class="btn btn-primary btn-main" type="submit">Speichern</button>
                </div>
            </form>
            <div class="margin-bottom-20 text-center">
                <a href="{{ route('login.get') }}">Zum Login</a>
            </div>
        </div>
    </div>
@endsection