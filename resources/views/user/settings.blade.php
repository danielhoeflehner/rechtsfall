@extends('layout')

@section('content')
    <div class="container-fluid margin-bottom-20">
        <div class="row main-header has-tabs">
            <div class="col-12">
                <h3>Deine Einstellungen</h3>
                <div class="header-subline">
                    Hier kannst du alle Einstellungen für dein Profil organisieren
                </div>
            </div>
            <div class="col-12 margin-top-20">
                <ul class="nav nav-tabs tabs-header" role="tablist">
                    <li class="nav-item">
                        <a href="#user-settings-general" class="nav-link @if($tab == 'general'){{ 'active' }}@endif" data-toggle="tab" role="tab" title="Allgemeine Einstellungen">Allgemein</a>
                    </li>
                    <li class="nav-item">
                        <a href="#user-settings-profile" class="nav-link @if($tab == 'profile'){{ 'active' }}@endif" data-toggle="tab" role="tab" title="Profil Einstellungen">Profil</a>
                    </li>
                    <li class="nav-item">
                        <a href="#user-settings-password" class="nav-link @if($tab == 'password'){{ 'active' }}@endif" data-toggle="tab" role="tab" title="Passwort Einstellungen">Passwort</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row breadcrumb-wrapper">
            <div class="col">
                <nav class="breadcrumb" aria-label="Du bist hier:" role="navigation">
                    <a class="breadcrumb-item" href="{{ route('home.get') }}">Home</a>
                    <span class="breadcrumb-item active">Einstellungen</span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="tab-content tank">
                    <div class="tab-pane @if($tab == 'general'){{ 'active' }}@endif" id="user-settings-general" role="tabpanel">
                        <div class="tank-header">
                            Allgemeine Einstellungen
                        </div>
                        <div class="tank-body">
                            <form action="{{ route('user.settings.general.post') }}" method="post">
                                <div class="row margin-bottom-20">
                                    <div class="col">
                                        <h5>Emails</h5>
                                        <p>Hier kannst du bestimmen, wann du eine Email Benachrichtigung bekommen willst.</p>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <label class="custom-control custom-checkbox">
                                            <?php
                                                $checked = '';

                                                if ($settings) {
                                                    $checked = $settings->us_emailPostGetsAnswer ? 'checked' : '';
                                                }
                                            ?>
                                            <input type="checkbox" class="custom-control-input" name="emailPostGetsAnswer" {{ $checked }}>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">... wenn ein Beitrag von mir eine Antwort bekommt.</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12">
                                        <label class="custom-control custom-checkbox">
                                            <?php
                                                $checked = '';

                                                if ($settings) {
                                                    $checked = $settings->us_emailPrivateMessageReceived ? 'checked' : '';
                                                }
                                            ?>
                                            <input type="checkbox" class="custom-control-input" name="emailPrivateMessageReceived" {{ $checked }}>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">... wenn ich eine private Nachricht eines anderen Users erhalte.</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row margin-bottom-20 margin-top-40">
                                    <div class="col">
                                        <h5>Notifications</h5>
                                        <p>Hier kannst du bestimmen, wann du Notifications erhalten willst.</p>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <label class="custom-control custom-checkbox">
                                            <?php
                                                $checked = '';

                                                if ($settings) {
                                                    $checked = $settings->us_notificationPostGetsAnswer ? 'checked' : '';
                                                }
                                            ?>
                                            <input type="checkbox" class="custom-control-input" name="notificationPostGetsAnswer" {{ $checked }}>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">... wenn ein Beitrag von mir eine Antwort bekommt.</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12">
                                        <label class="custom-control custom-checkbox">
                                            <?php
                                                $checked = '';

                                                if ($settings) {
                                                    $checked = $settings->us_notificationAnswerGetsVote ? 'checked' : '';
                                                }
                                            ?>
                                            <input type="checkbox" class="custom-control-input" name="notificationAnswerGetsVote" {{ $checked }}>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">... wenn eine Antwort up/down gevoted wurde.</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12">
                                        <label class="custom-control custom-checkbox">
                                            <?php
                                                $checked = '';

                                                if ($settings) {
                                                    $checked = $settings->us_notificationPrivateMessageReceived ? 'checked' : '';
                                                }
                                            ?>
                                            <input type="checkbox" class="custom-control-input" name="notificationPrivateMessageReceived" {{ $checked }}>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">... wenn ich eine private Nachricht eines anderen Users erhalte.</span>
                                        </label>
                                    </div>
                                </div>
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-primary margin-top-10" title="Einstellungen speichern">Speichern</button>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane @if($tab == 'profile'){{ 'active' }}@endif" id="user-settings-profile" role="tabpanel">
                        <div class="tank-header">
                            Persönliche Daten
                        </div>
                        <div class="tank-body">
                            <form action="{{ route('user.settings.profile.post') }}" method="post" enctype="multipart/form-data">
                                <div class="row form-group">
                                    <label class="col-sm-12 col-md-2 col-form-label" for="profile-image">Profil Bild</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input type="file" class="form-control-file" id="profile-image" name="profilePic" aria-describedby="fileHelp">
                                        <small id="fileHelp" class="form-text text-muted">Das Bild muss im Format jpeg oder png sein und darf nicht größer als 1 MB sein.</small>
                                        @if($errors->has('profilePic'))
                                            <div class="form-control-feedback">{{ $errors->first('profilePic') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group @if($errors->has('firstName')){{ 'has-warning' }}@endif">
                                    <label class="col-sm-12 col-md-2 col-form-label">Vorname*</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control @if($errors->has('firstName')){{ 'form-control-warning' }}@endif" type="text" name="firstName" value="{{ $user ? $user->u_firstName : old('firstName') }}" required>
                                        @if($errors->has('firstName'))
                                            <div class="form-control-feedback">{{ $errors->first('firstName') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group @if($errors->has('lastName')){{ 'has-warning' }}@endif">
                                    <label class="col-sm-12 col-md-2 col-form-label">Nachname*</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control @if($errors->has('lastName')){{ 'form-control-warning' }}@endif" type="text" name="lastName" value="{{ $user ? $user->u_lastName : old('lastName') }}" required>
                                        @if($errors->has('firstName'))
                                            <div class="form-control-feedback">{{ $errors->first('lastName') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group @if($errors->has('email')){{ 'has-warning' }}@endif">
                                    <label class="col-sm-12 col-md-2 col-form-label">Email*</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control @if($errors->has('email')){{ 'form-control-warning' }}@endif" type="email" name="email" value="{{ $user ? $user->email : old('email') }}" required>
                                        @if($errors->has('email'))
                                            <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group @if($errors->has('city')){{ 'has-warning' }}@endif">
                                    <label class="col-sm-12 col-md-2 col-form-label">Stadt</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control @if($errors->has('city')){{ 'form-control-warning' }}@endif" type="text" name="city" value="{{ !empty($user->u_city) ? $user->u_city : old('city') }}">
                                        @if($errors->has('city'))
                                            <div class="form-control-feedback">{{ $errors->first('city') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group @if($errors->has('job')){{ 'has-warning' }}@endif">
                                    <label class="col-sm-12 col-md-2 col-form-label">Beruf</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control @if($errors->has('job')){{ 'form-control-warning' }}@endif" type="text" name="job" value="{{ !empty($user->u_job) ? $user->u_job : old('job') }}">
                                        @if($errors->has('job'))
                                            <div class="form-control-feedback">{{ $errors->first('job') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group @if($errors->has('university')){{ 'has-warning' }}@endif">
                                    <label class="col-sm-12 col-md-2 col-form-label">Universität</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control @if($errors->has('university')){{ 'form-control-warning' }}@endif" type="text" name="university" value="{{ !empty($user->u_university) ? $user->u_university : old('university') }}">
                                        @if($errors->has('university'))
                                            <div class="form-control-feedback">{{ $errors->first('university') }}</div>
                                        @endif
                                    </div>
                                </div>
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-primary margin-top-10" title="Profil Daten speichern">Speichern</button>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane @if($tab == 'password'){{ 'active' }}@endif" id="user-settings-password" role="tabpanel">
                        <div class="tank-header">
                            Passwort ändern
                        </div>
                        <div class="tank-body">
                            <form action="{{ route('user.settings.password.post') }}" method="post">
                                <div class="row form-group @if($errors->has('oldPassword')){{ 'has-warning' }}@endif">
                                    <label class="col-sm-12 col-md-2 col-form-label">Altes Passwort</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control @if($errors->has('oldPassword')){{ 'form-control-warning' }}@endif" type="password" name="oldPassword">
                                        @if($errors->has('oldPassword'))
                                            <div class="form-control-feedback">{{ $errors->first('oldPassword') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group @if($errors->has('password')){{ 'has-warning' }}@endif">
                                    <label class="col-sm-12 col-md-2 col-form-label">Neues Passwort</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control @if($errors->has('password')){{ 'form-control-warning' }}@endif" type="password" name="password">
                                        @if($errors->has('password'))
                                            <div class="form-control-feedback">{{ $errors->first('password') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group @if($errors->has('password_confirmation')){{ 'has-warning' }}@endif">
                                    <label class="col-sm-12 col-md-2 col-form-label">Passwort wiederholen</label>
                                    <div class="col-sm-12 col-md-10">
                                        <input class="form-control @if($errors->has('password_confirmation')){{ 'form-control-warning' }}@endif" type="password" name="password_confirmation">
                                        @if($errors->has('password_confirmation'))
                                            <div class="form-control-feedback">{{ $errors->first('password_confirmation') }}</div>
                                        @endif
                                    </div>
                                </div>
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-primary margin-top-10" title="Neues Passwort speichern">Speichern</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection