@extends('layout')

@section('content')
    <div class="container-fluid">
        <div class="row main-header">
            <div class="col-8">
                <h3>Dein Profil</h3>
                <div class="header-subline">
                    Hier hast du einen Überblick über dein Profil
                </div>
            </div>
            <div class="col-4 text-right">
                <a href="{{ route('user.settings.get', ['tab' => 'profile']) }}" class="btn btn-primary" title="Profil bearbeiten"><i class="fa fa-pencil"></i></a>
                <a href="{{ route('user.settings.get', ['tab' => 'password']) }}" class="btn btn-danger" title="Passwort ändern"><i class="fa fa-lock"></i></a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row breadcrumb-wrapper">
            <div class="col">
                <nav class="breadcrumb" aria-label="Du bist hier:" role="navigation">
                    <a class="breadcrumb-item" href="{{ route('home.get') }}">Home</a>
                    <span class="breadcrumb-item active">Dein Profil</span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-5 margin-bottom-20">
                <div class="tank profile-data-wrapper">
                    <div class="profile-data-header">
                        <div class="name">
                            {{ $user->getFullName() }}
                        </div>
                        <img src="{{ route('user.profilepic.get') }}" class="pic" alt="Profilbild {{ $user->getFullName() }}" title="Dein Profilbild">
                    </div>
                    <div class="container-fluid profile-data-body">
                        <ul>
                            <li>
                                <div class="row">
                                    <div class="col-sm-3 col-md-4 text-sm-right">
                                        Email
                                    </div>
                                    <div class="col-sm-9 col-md-8">
                                        {{ $user->email }}
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-sm-3 col-md-4 text-sm-right">
                                        Beruf
                                    </div>
                                    <div class="col-sm-9 col-md-8">
                                        {{ !empty($user->u_job) ? $user->u_job : 'Was machst du beruflich?' }}
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-sm-3 col-md-4 text-sm-right">
                                        Stadt
                                    </div>
                                    <div class="col-sm-9 col-md-8">
                                        {{ !empty($user->u_city) ? $user->u_city : 'Wo studierst/arbeitest du?' }}
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-sm-3 col-md-4 text-sm-right">
                                        Universität
                                    </div>
                                    <div class="col-sm-9 col-md-8">
                                        {{ !empty($user->u_university) ? $user->u_university : 'Welche Universität besuchst du?' }}
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="text-center">
                        <a href="{{ route('user.settings.get', ['tab' => 'profile']) }}" class="btn btn-primary btn-sm" title="Profil bearbeiten"><i class="fa fa-pencil"></i> Bearbeiten</a>
                        <a href="{{ route('user.settings.get', ['tab' => 'password']) }}" class="btn btn-danger btn-sm" title="Passwort ändern"><i class="fa fa-lock"></i> Passwort ändern</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-7">
                <div class="tank margin-bottom-20">
                    <div class="tank-header">
                        Deine letzten Beiträge
                    </div>
                    <div class="column-body">

                    </div>
                </div>
                <div class="tank">
                    <div class="tank-header">
                        Deine letzten Kommentare
                    </div>
                    <div class="tank-body">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection