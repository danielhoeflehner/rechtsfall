<form id="login-form" class="form" method="post" action="/password/email">
    <div class="my-form-control {{ $errors->has('email') ? 'error-input' : '' }}" style="margin-bottom: 10px;">
        <input type="text" name="email" value="{{ old('email') }}" required>
        <div class="underline"></div>
        <label for="first_name">Email</label>
    </div>
    {{ csrf_field() }}
    <button class="btn btn-primary btn-block margin-top-20" type="submit" name="btn_register">Neues Passwort beantragen</button>
    <div class="clearfix margin-top-10">
        <div class="float-left">
            <a href="/register">Registrieren</a>
        </div>
        <div class="float-right text-right">
            <a href="/login">Login</a>
        </div>
    </div>
</form>