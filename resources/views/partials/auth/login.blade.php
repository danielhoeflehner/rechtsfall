<form id="login-form" class="form" method="post" action="/login">
    <div class="my-form-control{{ $errors->has('email') ? ' error-input' : '' }}">
        <input type="text" name="email" value="{{ old('email') }}" required>
        <div class="underline"></div>
        <label for="email">Email</label>
    </div>
    <div class="my-form-control{{ $errors->has('password') ? ' error-input' : '' }}">
        <input type="password" name="password" required>
        <div class="underline"></div>
        <label for="password">Passwort</label>
    </div>
    {{ csrf_field() }}
    <button class="btn btn-primary btn-block" type="submit" name="btn_save">Einloggen</button>
    <div class="clearfix margin-top-10">
        <div class="float-left">
            <a href="/register">Registrieren</a>
        </div>
        <div class="float-right text-right">
            <a href="/password/reset">Passwort vergessen</a>
        </div>
    </div>
</form>
