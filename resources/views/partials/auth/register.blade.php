<form id="login-form" class="form" method="post" action="/register" autocomplete="off">
    <div class="my-form-control{{ $errors->has('firstName') ? ' error-input' : '' }}">
        <input type="text" name="firstName" value="{{ old('firstName') }}" required>
        <div class="underline"></div>
        <label for="first_name">Vorname</label>
    </div>
    <div class="my-form-control{{ $errors->has('lastName') ? ' error-input' : '' }}">
        <input type="text" name="lastName" value="{{ old('lastName') }}" required>
        <div class="underline"></div>
        <label for="last_name">Nachname</label>
    </div>
    <div class="my-form-control{{ $errors->has('email') ? ' error-input' : '' }}">
        <input type="text" name="email" value="{{ old('email') }}" required>
        <div class="underline"></div>
        <label for="email">Email</label>
    </div>
    <div class="my-form-control{{ $errors->has('password') ? ' error-input' : '' }}">
        <input type="password" name="password" required>
        <div class="underline"></div>
        <label for="password">Passwort</label>
    </div>
    <div class="my-form-control{{ $errors->has('password_confirmation') ? ' error-input' : '' }}">
        <input type="password" name="password_confirmation" required>
        <div class="underline"></div>
        <label for="password_confirm">Passwort bestätigen</label>
    </div>
    {{ csrf_field() }}
    <button class="btn btn-primary btn-block" type="submit" name="btn_register">Registrieren</button>
    <div class="clearfix margin-top-10">
        <div class="float-left">
            <a href="/login">Login</a>
        </div>
        <div class="float-right text-right">

        </div>
    </div>
</form>