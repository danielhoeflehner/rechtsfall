<form id="login-form" class="form" method="post" action="/password/reset">
    <div class="my-form-control{{ $errors->has('email') ? ' error-input' : '' }}">
        <input type="text" name="email" value="{{ old('email') }}" required>
        <div class="underline"></div>
        <label for="email">Email</label>
    </div>
    <div class="my-form-control{{ $errors->has('password') ? ' error-input' : '' }}">
        <input type="password" name="password" required>
        <div class="underline"></div>
        <label for="password">Passwort</label>
    </div>
    <div class="my-form-control{{ $errors->has('password_confirmation') ? ' error-input' : '' }}">
        <input type="password" name="password_confirmation" required>
        <div class="underline"></div>
        <label for="password">Passwort bestätigen</label>
    </div>
    <input type="hidden" name="token" value="{{ $token }}">
    {{ csrf_field() }}
    <button class="btn btn-primary btn-block" type="submit" name="btn_save">Einloggen</button>
    <div class="clearfix margin-top-10">
        <div class="float-left">
            <a href="/login">Login</a>
        </div>
        <div class="float-right text-right">
            <a href="/register">Registrieren</a>
        </div>
    </div>
</form>
