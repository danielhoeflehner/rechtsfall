<div class="row align-items-center">
    <div class="col-2">
        <div id="nav-toggle" title="Navigation ein/ausblenden">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="col-10">
        <ul class="list-inline menu float-right">
            <li class="list-inline-item" id="search-wrapper">
                <input id="search" type="text" placeholder="Suche ...">
                <div class="search-underline"></div>
                <div id="search-result-desktop">
                </div>
            </li><!--
            --><li class="list-inline-item" id="search-sign-mobile"><a href="#" title="Suche ..."><i class="lnr lnr-magnifier"></i></a></li><!--
            --><li class="list-inline-item"><a href="/notification" title="Benachrichtigungen"><i class="lnr lnr-alarm lnr-lg"></i></a></li><!--
            --><li class="list-inline-item"><a href="{{ route('user.settings.get') }}" title="Einstellungen"><i class="lnr lnr-cog lnr-lg"></i></a></li><!--
            --><li class="list-inline-item"><a href="{{ route('logout.get') }}" title="Logout"><i class="lnr lnr-power-switch lnr-lg"></i></a></li>
        </ul>
    </div>
</div>
<div id="search-wrapper-mobile">
    <input id="search-mobile" type="text" placeholder="Suche ...">
    <div class="search-underline"></div>
    <div id="search-result-mobile">
    </div>
</div>