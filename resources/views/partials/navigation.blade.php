<nav id="nav-sidebar">
    <div class="brand">
        rechtsfall.at
    </div>
    <div class="user-nav">
        <img src="{{ route('user.profilepic.get') }}" alt="{{ Auth::user()->getFullName() }}" class="pic">
        <div class="name"><a href="{{ route('user.profile.get') }}" title="Zu deinem Profil" >{{ Auth::user()->getFullName() }}</a></div>
        {{ Auth::user()->u_job }}
    </div>
    <ul id="main-nav">
        <li class="@if($nav == App\Models\Navigation::NAV_HOME) active @endif">
            <a href="{{ route('home.get') }}"><i class="lnr lnr-home"></i>Home</a>
        </li>
        <li class="@if($nav == App\Models\Navigation::NAV_POSTS) active @endif">
            <a href="{{ route('post.get') }}"><i class="lnr lnr-layers"></i>Alle Beiträge</a>
        </li>
        <li class="@if($nav == App\Models\Navigation::NAV_POST_NEW) active @endif">
            <a href="{{ route('post.create.get') }}"><i class="lnr lnr-plus-circle"></i>Neuer Beitrag</a>
        </li>
        <li class="@if($nav == App\Models\Navigation::NAV_INBOX) active @endif">
            <a href="/user/inbox"><i class="lnr lnr-envelope"></i>Nachrichten <span class="badge badge-pill badge-danger pull-right" style="margin-right: 10px;">13</span></a>
        </li>
        <li class="@if($nav == App\Models\Navigation::NAV_PROFILE) active @endif">
            <a href="{{ route('user.profile.get') }}"><i class="lnr lnr-user"></i>Profil</a>
        </li>
        <li class="@if($nav == App\Models\Navigation::NAV_SETTINGS) active @endif">
            <a href="{{ route('user.settings.get') }}"><i class="lnr lnr-cog"></i>Einstellungen</a>
        </li>
        @if(Auth::user()->isSuperuser())
            <li class="sub-menu">
                <a><i class="lnr lnr-diamond"></i>Admin<i class="fa @if($nav == App\Models\Navigation::NAV_ADMIN) fa-angle-up @else fa-angle-down @endif fa-lg pull-right"></i></a>
            </li>
            <ul class="sub-nav @if($nav == App\Models\Navigation::NAV_ADMIN) open @endif">
                <li class="@if($subnav == App\Models\Navigation::NAV_ADMIN_SETTINGS) active @endif"><a href="/admin/settings"><i class="fa fa-sliders"></i>Overview</a></li>
                <li class="@if($subnav == App\Models\Navigation::NAV_ADMIN_USER) active @endif"><a href="/admin/user"><i class="fa fa-sliders"></i>User</a></li>
                <li class="@if($subnav == App\Models\Navigation::NAV_ADMIN_POST) active @endif"><a href="/admin/post"><i class="fa fa-sliders"></i>Beiträge</a></li>
            </ul>
        @endif
    </ul>
</nav>