<?php

$isDev = App::environment('local');
$isAuth = Auth::check();

?>
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Rechtsfälle gemeinsam lösen</title>

        <link href="https://fonts.googleapis.com/css?family=Roboto:200,300,400,600,700" rel="stylesheet">
        @if(!$isDev)
            <link rel="stylesheet" href="//cdn.quilljs.com/1.1.7/quill.snow.css">
            <link rel="stylesheet" href="/css/style.min.css">
        @else
            <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.css">
            <link rel="stylesheet" href="/node_modules/quill/dist/quill.snow.css">
            <link rel="stylesheet" href="/bower_components/linearicons/dist/web-font/style.css">
            <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.css">
            <link rel="stylesheet" href="/css/style.css">
        @endif
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>
    <body>
        @component('components.alert') Info @endcomponent
        @component('components.error') Achtung @endcomponent
        @auth
            <header class="container-fluid" id="top-nav">
                @include('partials.header')
            </header>
        @endauth
        <section class="container-fluid main-wrapper">
            @auth
                @include('partials.navigation')
            @endauth
            <div id="content-wrapper" class="@if($isAuth) content-wrapper @else content-wrapper-auth @endif">
                @yield('content')
            </div>
        </section>
        <div id="cookie-reminder">
            <p>
                Yo, Cookies und so.<br>
            </p>
            <button class="button warning">Alrighty.</button>
        </div>
        <script src="/bower_components/jquery/dist/jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="/bower_components/bootstrap/dist/js/bootstrap.js"></script>
        <script src="/node_modules/quill/dist/quill.js"></script>
        <script src="/js/main.js"></script>
        @yield('scripts')
    </body>
</html>
