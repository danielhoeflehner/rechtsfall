<div class="flash-wrapper">
    @foreach(['danger', 'success', 'warning', 'info'] as $key)
        @if(session()->has($key))
            <div class="flash-alert">
                <div class="alert alert-{{ $key }}">
                    <div class="row">
                        <div class="col-2 text-center">
                            <span class="lnr lnr-rocket lnr-2x"></span>
                        </div>
                        <div class="col-10">
                            <div class="alert-headline">{{ $slot }}</div>
                            <div class="alert-body">
                                {{ session()->get($key) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
</div>