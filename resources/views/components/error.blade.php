<div class="flash-wrapper">
    @if($errors->count() > 0)
        <div class="flash-alert">
            <div class="alert alert-danger">
                <div class="row">
                    <div class="col-2 text-center">
                        <span class="lnr lnr-warning lnr-2x"></span>
                    </div>
                    <div class="col-10">
                        <div class="alert-headline">{{ $slot }}</div>
                        <div class="alert-body">
                            @foreach($errors->all() as $error)
                                <div class="margin-bottom-10">{{ $error }}</div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>