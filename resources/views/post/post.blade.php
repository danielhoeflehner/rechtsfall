<?php

$authUser = auth()->user();

?>

@extends('layout')

@section('scripts')
    <script src="/js/post.js"></script>
    <script src="/js/answers.js"></script>
@endsection

@section('content')
    <div class="container-fluid margin-bottom-20">
        <div class="row main-header">
            <div class="col">
                <h3>Beitrag</h3>
                <div class="header-subline">
                    Hier kannst du mithelfen, den Fall zu lösen.
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row breadcrumb-wrapper">
            <div class="col">
                <nav class="breadcrumb" aria-label="Du bist hier:" role="navigation">
                    <a class="breadcrumb-item" href="/home">Start</a>
                    <a class="breadcrumb-item" href="/post">Alle Beiträge</a>
                    <span class="breadcrumb-item active">Beitrag</span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="tank post-wrapper">
                    <div class="tank-header post-header">
                        <div class="info">
                            {{ $post->formattedCreatedAt }} von <a href="#">{{ $post->user->getFullName() }}</a> <span class="xp"> 3000 rsc</span>
                        </div>
                        @if($authUser->isSuperuser())
                            <span class="small">ID: {{ $post->getKey() }}</span>
                        @endif
                        <div class="title">
                            {{ $post->getTitle() }}
                        </div>
                        <div class="tags">
                            @foreach($post->getTags() as $tag)
                                <a href="{{ route('tag.posts.get', ['tag' => $tag['id']]) }}"><span class="badge badge-pill badge-primary">{{ $tag['value'] }}</span></a>
                            @endforeach
                        </div>
                    </div>
                    <div class="tank-body ql-editor">
                        {!! trim($post->getBody()) !!}
                    </div>
                    <div class="tank-footer">
                        @if($authUser->getKey() == $post->user->getKey() && !$post->isConfirmed())
                            <button id="post-set-solved" class="btn btn-primary" data-pid="{{ $post->getKey() }}">
                                <span>Fall als gelöst markieren<span>
                            </button>
                        @elseif($post->isConfirmed())
                            <span class="lnr lnr-checkmark-circle"></span> <b>Fall wurde {{ $post->formattedConfirmedAt }} als gelöst markiert.</b>
                        @else
                            <span class="lnr lnr-cross-circle"></span> Fall ist noch nicht gelöst.
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="post-answers">
            <div class="col margin-top-20">
                <div class="tank">
                    <div class="tank-header">
                        Antworten
                    </div>
                    <div class="tank-body">
                        @forelse($answers as $answer)
                            <?php
                                $pa_id = $answer->getKey();
                                $upCount = $answer->getUpVotesCount();
                                $downCount = $answer->getDownVotesCount();

                                $alreadyVoted = $authUser->alreadyVoted($pa_id);
                            ?>
                            <div class="row answer__wrapper">
                                <div class="col-md-2 col-xl-1 text-center margin-bottom-20">
                                    <img class="pic pic-profile-sm" src="{{ route('user.profilepic.get') }}">
                                    <div class="answer__vote">
                                        <button id="btn-up-{{ $pa_id }}" class="btn btn-outline-info answer__vote-btn up"
                                                data-paid="{{ $pa_id }}"
                                                data-pid="{{ $post->getKey() }}"
                                                data-count="{{ $upCount }}"
                                                title="{{ $alreadyVoted ? 'Für diese Antwort hast du schon abgestimmt.' : 'Stimme der Antwort zu.' }}"
                                                @if($alreadyVoted) disabled @endif>
                                            <span class="lnr lnr-chevron-up lnr-lg"></span>
                                        </button>
                                        <div id="counter-{{ $pa_id }}" class="vote-count">
                                            {{ $answer->ranking }}
                                        </div>
                                        <button id="btn-down-{{ $pa_id }}" class="btn btn-outline-info answer__vote-btn down"
                                                data-paid="{{ $pa_id }}"
                                                data-pid="{{ $post->getKey() }}"
                                                data-count="{{ $downCount }}"
                                                title="{{ $alreadyVoted ? 'Für diese Antwort hast du schon abgestimmt.' : 'Stimme der Antwort nicht zu.' }}"
                                                @if($alreadyVoted) disabled @endif>
                                            <span class="lnr lnr-chevron-down lnr-lg"></span>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-10 col-xl-11">
                                    <div class="answer__header text-center text-md-left">
                                        <span class="date">{{ $answer->formattedCreatedAt }}</span> von <a href="#">{{ $answer->user->getFullName() }}</a> <span class="xp"> 3000 rsc</span>
                                        @if($authUser->isSuperuser())
                                            <span class="small">ID: {{ $answer->getKey() }}</span>
                                        @endif
                                    </div>
                                    <div class="answer__body ql-editor">
                                        {!! $answer->getBody() !!}
                                    </div>
                                </div>
                            </div>
                        @empty
                            Zu diesem Fall gibt es noch keine Lösungen.
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col margin-top-20">
                <div class="tank">
                    <div class="tank-header">
                        Neue Antwort schreiben
                    </div>
                    <form class="answer" id="post-form" action="/post/{{ $post->getKey() }}/answer/store" method="post">
                        <div class="tank-body">
                            <input type="hidden" class="form-control" id="post-content-html" name="answer_content_html">
                            <div id="post-content">
                            </div>
                            {{ csrf_field() }}
                            <div class="float-right">
                                <button type="submit" class="btn btn-primary btn-main">Speichern</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection