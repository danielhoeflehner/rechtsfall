<?php

$authUser = Auth::user();

?>

@extends('layout')

@section('content')
    <div class="container-fluid margin-bottom-20">
        <div class="row main-header">
            <div class="col">
                <h3>Neuer Beitrag</h3>
                <div class="header-subline">
                    Hier kannst du einen neuen Beitrag erstellen
                </div>
            </div>
        </div>
        <div class="row breadcrumb-wrapper">
            <div class="col">
                <nav class="breadcrumb" aria-label="Du bist hier:" role="navigation">
                    <a class="breadcrumb-item" href="{{ route('home.get') }}">Start</a>
                    <span class="breadcrumb-item active">Alle Beiträge</span>
                </nav>
            </div>
        </div>
        <div class="row">
            @foreach($posts as $post)
                <div class="col-md-6 col-lg-4 col-xl-3 margin-bottom-20">
                    <div class="tank {{ $post->isConfirmed() ? 'bt-green' : 'bt-red' }}">
                        <div class="tank-body post-preview-wrapper">
                            <div class="post-header">
                                <div class="info">
                                    {{ $post->formattedCreatedAt }} von <a href="{{ route('user.get', ['user' => $post->user->getKey()]) }}">{{ $post->user->u_firstName }}</a>
                                </div>
                                <div class="title">
                                    <a href="{{ route('post.show.get', ['post' => $post->getKey()]) }}">{{ $post->p_title }}</a>
                                </div>
                                <div class="tags">
                                    @foreach($post->getTags() as $tag)
                                        <a href="{{ route('tag.posts.get', ['tag' => $tag['id']]) }}"><span class="badge badge-pill badge-primary">{{ $tag['value'] }}</span></a>
                                    @endforeach
                                </div>
                            </div>
                            <div class="post-body">
                                {{ $post->shortenedBody }}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col text-center margin-top-10">
                {{ $posts->links('vendor.pagination.bootstrap-4') }}
            </div>
        </div>
    </div>
@endsection