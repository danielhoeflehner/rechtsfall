<?php

    $url = isset($post) ? route('post.update.post', ['post' => $post->getKey()]) : route('post.create.post');

?>

@extends('layout')

@section('scripts')
    <script src="/js/taggy.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function (event) {
            var taggy = new Taggy('#post-tags' , {
                url: '/tag/search',
                method: 'POST'
            }).init();
        });
    </script>
@endsection

@section('content')
    <div class="container-fluid margin-bottom-20">
        <div class="row main-header">
            <div class="col">
                <h3>Neuer Beitrag</h3>
                <div class="header-subline">
                    Hier kannst du einen neuen Beitrag erstellen
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row breadcrumb-wrapper">
            <div class="col">
                <nav class="breadcrumb" aria-label="Du bist hier:" role="navigation">
                    <a class="breadcrumb-item" href="/home">Home</a>
                    <span class="breadcrumb-item active">Neuer Beitrag</span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="tab-content tank">
                    <div class="tank-body">
                        <form id="post-form" action="{{ $url }}" method="post">
                            <div class="form-group">
                                <label for="post-title">Titel</label>
                                <input type="text"
                                       class="form-control"
                                       id="post-title"
                                       name="post_title"
                                       placeholder="Beschreib kurz um was es geht ..."
                                       value="{{ $post->p_title or old('post_title') }}"
                                       maxlength="100"
                                       required
                                >
                            </div>
                            <div id="post-tags" class="form-group">
                                <label for="post-tags">Tags (max. 5)</label>
                                <div class="post-tags-wrapper">
                                    @if(isset($post))
                                        @foreach($post->tags as $key => $tag)
                                            <span class="tag-item">
                                                {{ $tag }} <i class="lnr lnr-cross" title="Tag löschen"></i>
                                                <input type="hidden" name="post_tags[][{{ $key }}]" value="{{ $tag }}">
                                            </span>
                                        @endforeach
                                    @endif
                                    <input type="text" id="tag-input" autocomplete="off" autofocus>
                                    <div id="tag-sug">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-content-input">Beitrag</label>
                                <input type="hidden" class="form-control" id="post-content-html" name="post_content_html" required>
                                <div id="post-content">
                                    {!! $post->p_body or old('post_content_html') !!}
                                </div>
                            </div>
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-primary btn-main">Speichern</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection