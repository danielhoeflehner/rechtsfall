@extends('layout')

@section('content')
    <div class="container-fluid">
        <div class="row main-header">
            <div class="col-sm-12 col-md-8">
                <h3>Willkommen {{ Auth::user()->u_firstName }}</h3>
                <div class="header-subline">
                    Hier siehst du einen Überblick
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <a class="btn-new-post float-right" href="{{ route('post.create.get') }}" title="Neuen Post erstllen">
                    <i class="fa fa-plus fa-2x"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row breadcrumb-wrapper">
            <div class="col">
                <nav class="breadcrumb" aria-label="Du bist hier:" role="navigation">
                    <span class="breadcrumb-item active">Start</span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-8 margin-bottom-30">
                <div class="tank">
                    <div class="tank-header">
                        Neueste Beiträge
                    </div>
                    <div class="tank-body">
                        @foreach($posts as $post)
                            <div class="post-wrapper preview">
                                <div class="post__status @if($post->isConfirmed()) solved @else not-solved @endif">
                                    <div class="post__header">
                                        <div class="info">
                                            {{ $post->formattedCreatedAt }} von <a href="{{ route('user.get', ['user' => $post->user->getKey()]) }}">{{ $post->user->getFullName() }}</a> &bullet; <b>{{ $post->getFormattedAnswerCount() }}</b>
                                        </div>
                                        <div class="title">
                                            <a href="{{ route('post.show.get', ['post' => $post->getKey()]) }}">{{ $post->p_title }}</a>
                                        </div>
                                        <div class="tags">
                                            @foreach($post->getTags() as $tag)
                                                <a href="{{ route('tag.posts.get', ['tag' => $tag['id']]) }}"><span class="badge badge-pill badge-primary">{{ $tag['value'] }}</span></a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="post__body">
                                        {{ $post->shortenedBody }}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-4">
                <div class="tank">
                    <div class="tank-header">
                        Benachrichtigungen
                    </div>
                    <div class="tank-body no-padd">
                        @forelse($notifications as $notification)
                            <div class="row no-gutters notification">
                                <div class="col-sm-2">
                                    <div class="notification__header bad">
                                        <i class="lnr lnr-rocket lnr-2x"></i>
                                    </div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="notification__body">
                                        <a href="{{ $notification->n_description['link'] }}" title="direkt zum Beitrag">
                                            {!! $notification->n_description['text'] !!}
                                        </a>
                                        <div class="date">{{ $notification->formattedCreated }}</div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="notification">
                                <a href="#">
                                    Im Moment gibt es keine Benachrichtigungen.
                                </a>
                            </div>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
