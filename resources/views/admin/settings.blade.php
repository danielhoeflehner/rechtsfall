@extends('layout')

@section('scripts')
    <script>
        Echo.channel('user.logged.in')
            .listen('Login', (e) => {
                console.log(e);
            });
    </script>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row main-header">
            <div class="col">
                <h3>Dein Profil</h3>
                <div class="header-subline">
                    Hier hast du einen Überblick über dein Profil
                </div>
            </div>
        </div>
        <div class="row breadcrumb-wrapper">
            <div class="col">
                <nav class="breadcrumb" aria-label="Du bist hier:" role="navigation">
                    <a class="breadcrumb-item" href="/home">Home</a>
                    <span class="breadcrumb-item active">Dein Profil</span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col margin-bottom-20">
                <div id="logged-in-users">

                </div>
            </div>
        </div>
    </div>
@endsection