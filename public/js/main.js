'use strict';

document.addEventListener('DOMContentLoaded', function (event) {

    initQuillEditor();

    var flash_wrapper   = document.getElementsByClassName('flash-wrapper')[0];
    var nav_toggle      = document.getElementById('nav-toggle');
    var nav_sidebar     = document.getElementById('nav-sidebar');
    var content_wrapper = document.getElementById('content-wrapper');
    var sub_menus       = nav_sidebar ? nav_sidebar.getElementsByClassName('sub-menu') : null;
    var cookie_reminder = document.getElementById('cookie-reminder');
    var btn_search      = document.getElementById('search-sign-mobile');

    if (typeof(Storage) !== 'undefined') {
        if (!localStorage.visited) {
            cookie_reminder.style.display = 'block'
        }
    }

    if (cookie_reminder.style.display == 'block') {
        var btn = cookie_reminder.querySelector('button');

        btn.addEventListener('click', function (event) {
            localStorage.setItem('visited', true);
            cookie_reminder.style.display = 'none';
        });
    }

    //$('[data-toggle="tooltip"]').tooltip();

    if (flash_wrapper) {
        /*setTimeout(function () {
            flash_wrapper.remove();
        }, 4000);*/
    }

    if (nav_toggle) {
        nav_toggle.addEventListener('click', function (event) {
            toggleNavSidebar(nav_sidebar, content_wrapper);
        });
    }

    if (sub_menus) {
        Array.prototype.forEach.call(sub_menus, function (sub_menu) {
            sub_menu.addEventListener('click', function (event) {
                var sub_menu_clicked = this;
                var sub_nav = sub_menu_clicked.nextElementSibling;

                toggleSubNav(sub_menu_clicked, sub_nav);
            });
        });
    }

    if (btn_search) {
        var search_mobile = document.getElementById('search-wrapper-mobile');
        btn_search.addEventListener('click', function (event) {
            event.preventDefault();

            search_mobile.classList.toggle('show-search');
        });
    }


});

function toggleNavSidebar(nav, content) {
    var nav_style = window.getComputedStyle(nav, null);
    var isMobile = window.innerWidth < 768;

    if (nav_style.getPropertyValue('margin-left') == '0px') {
        nav.style.marginLeft = '-200px';
        if (!isMobile) {
            content.style.marginLeft = '0px';
        }
    } else {
        nav.style.marginLeft = '0px';
        if (!isMobile) {
            content.style.marginLeft = '200px';
        }
    }
}

function toggleSubNav(sub_menu, sub_nav) {
    // Get the second i element
    var i = sub_menu.getElementsByTagName('i')[1];

    if (sub_nav.classList.contains('open')) {
        i.classList.remove('fa-angle-up');
        i.classList.add('fa-angle-down');
        sub_nav.classList.remove('open');
    } else {
        i.classList.remove('fa-angle-down');
        i.classList.add('fa-angle-up');
        sub_nav.classList.add('open');
    }
}

function initQuillEditor() {

    var post_form = document.getElementById('post-form');

    if (! post_form) {
        return;
    }

    var toolbarOptions = [
        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
        ['blockquote', 'code-block'],

        [{ 'header': 1 }, { 'header': 2 }],               // custom button values
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
        [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent

        [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown

        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
        [{ 'align': [] }],

        ['clean']                                         // remove formatting button
    ];

    var post_content_html = document.getElementById('post-content-html');
    var quill = new Quill('#post-content', {
        modules: {
            toolbar: toolbarOptions
        },
        theme: 'snow'
    });

    post_form.addEventListener('submit', function (event) {
        post_content_html.value = quill.root.innerHTML;
    });
}

window.XHR = function (method, url, dataType, data, callback) {
    var xhr = new XMLHttpRequest();

    xhr.open(method, url);

    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.setRequestHeader("X-CSRF-TOKEN", Laravel.csrfToken);
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    xhr.responseType = dataType;

    xhr.onreadystatechange = function () {
        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
            callback(xhr.response);
        }
    }

    xhr.send(JSON.stringify(data));
}