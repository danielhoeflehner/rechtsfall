'use strict';

document.addEventListener('DOMContentLoaded', function (event) {

    let inputs = document.querySelectorAll('input');
    let loginInfo = document.getElementById('login-info');

    if (inputs) {
        inputs.forEach(function (input) {
            input.addEventListener('click', function (event) {
                let input = this;
                let label = document.querySelector('label[for=' + input.id + ']');

                label.classList.add('selected');
            });

            input.addEventListener('blur', function (event) {
                let input = this;
                let label = document.querySelector(('label[for=' + input.id + ']'));

                label.classList.remove('selected');
            });
        });
    }

    if (loginInfo) {
        let btn = loginInfo.querySelector('.login-info-btn');

        btn.addEventListener('click', function (event) {
            toggleLoginInfo(loginInfo, btn);
        });
    }

});

function toggleLoginInfo(box, btn)
{
    let span = btn.querySelector('span');

    if (span.classList.contains('up')) {
        box.style.transform = "translateY(-242px)";
        span.classList.remove('up');
    } else {
        box.style.transform = "translateY(0)";
        span.classList.add('up');
    }
}