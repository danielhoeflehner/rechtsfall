'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var copy = require('gulp-copy');
var browserSync = require('browser-sync').create();

gulp.task('sass', function () {
    return gulp.src('../resources/assets/sass/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('minify-css', ['sass'], function () {
    return gulp.src(['./css/*.css', '!./css/*.min.css'])
        .pipe(concat('style.css'))
        .pipe(cleanCSS())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./css'));
});


gulp.task('copy', function () {
    return gulp.src(['../resources/assets/js/*.js', '!../resources/assets/js/*.not.js, !../resources/assets/js/app.js, !../resources/assets/js/bootstrap.js'])
        .pipe(gulp.dest('./js'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('browserSync', function () {

});

gulp.task('watch', ['sass'], function () {
    browserSync.init({
        proxy: "rechtsfall.dev"
    });

    gulp.watch('../resources/assets/sass/**/*.scss', ['sass']);
    gulp.watch('../resources/views/**/*.php', browserSync.reload);
    gulp.watch(['../resources/assets/js/*.js', '!../resources/assets/js/*.not.js'], ['copy']);
});