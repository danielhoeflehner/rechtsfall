<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;
use App\Models\Post;

class PostGotAnswered extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    private $post;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param Post $post
     */
    public function __construct(User $user, Post $post)
    {
        $this->user = $user;
        $this->post = $post;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.post.answered')
            ->with([
                'name' => $this->user->getFullName(),
                'post' => $this->post->getKey()
            ]);
    }
}
