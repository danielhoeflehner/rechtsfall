<?php

namespace App\Models;

class Navigation
{
    const NAV_HOME = 'home';

    const NAV_POSTS = 'posts';

    const NAV_POST_NEW = 'post_new';

    const NAV_PROFILE = 'profile';

    const NAV_SETTINGS = 'settings';

    const NAV_INBOX = 'inbox';

    const NAV_ADMIN = 'admin';
    const NAV_ADMIN_SETTINGS = 'admin_settings';
    const NAV_ADMIN_USER = 'admin_user';
    const NAV_ADMIN_POST = 'admin_post';
}
