<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Auth;

class Tag extends BaseModel
{
    use SoftDeletes;

    protected $table = 'tbl_tag';
    protected $primaryKey = 't_id';

    protected $casts = [
        't_count' => 'integer'
    ];

    protected $guarded = [];

    const TAG_NEW = 'new';

    /*
     * ------------------------------ BOOT ------------------------------------
     */


    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->t_count      = 1;
            $model->t_identifier = self::formatIdentifier($model->t_name);
            $model->fk_u_id      = Auth::id();
        });
    }


    /*
     * ------------------------------ SCOPES ----------------------------------
     */

    public function scopeFindByIdentifier($query, string $identifier)
    {
        return $query->where('t_identifier', $identifier);
    }

    /*
     * ---------------------------- RELATIONS ---------------------------------
     */

    public function user()
    {
        return $this->belongsTo(User::class, 'fk_u_id');
    }

    /*
     * ----------------------------- FUNCTIONS --------------------------------
     */

    public static function formatIdentifier(string $name) : string
    {
        return str_replace([' ', '-'], ['_', '_'], strtolower(trim($name)));
    }

    public static function createOrUpdateTag($key, string $value) : Model
    {
        $tag = null;

        if ($key == self::TAG_NEW) {
            $identifier = self::formatIdentifier($value);

            $tag = self::findByIdentifier($identifier)->first();
            if (!$tag) {
                $tag = self::create(['t_name' => $value]);
            } else {
                $tag->update(['t_count' => $tag->t_count + 1]);
            }
        } else if (is_numeric($key)) {
            $tag = self::find($key);
            if ($tag) {
                $tag->update(['t_count' => $tag->t_count + 1]);
            }
        }

        return $tag;
    }

    public function getIdentifier() : string
    {
        return $this->t_identifier;
    }

    public function getName() : string
    {
        return $this->t_name;
    }

    public function getCount() : int
    {
        return $this->t_count;
    }
}
