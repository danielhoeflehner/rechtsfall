<?php

namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class LoginLog extends BaseModel
{
    protected $table = 'tbl_loginLog';
    protected $primaryKey = 'll_id';

    protected $casts = [
        'll_desktop' => 'boolean',
        'll_mobile' => 'boolean',
        'll_tablet' => 'boolean',
        'll_phone' => 'boolean'
    ];

    use SoftDeletes;

    /*
     * --------------------------------------------------------------------
     */
}
