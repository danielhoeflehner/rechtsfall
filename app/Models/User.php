<?php

declare(strict_types=1);

namespace App\Models;

use App\Scopes\ActiveUserScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPasswordNotification;

use Cache;
use Closure;
use Hash;

class User extends Authenticatable
{
    protected $primaryKey = 'u_id';
    protected $table = 'tbl_user';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $guarded = [
        'password',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'u_superuser'   => 'boolean',
        'u_active'      => 'boolean'
    ];

    use Notifiable, SoftDeletes;

    /*
     * --------------------------- CONSTANTS ----------------------------------
     */

    const DEFAULT_PROFILE_PICTURE = 'user_profilepic_default.jpg';
    const CACHE_KEY_ANSWER_VOTED = 'user.###UID###.postanswer.###PAID###.voted';

    /*
     * ------------------------------ BOOT ------------------------------------
     */

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveUserScope);
    }

    /*
     * ---------------------------- RELATIONS ---------------------------------
     */

    public function posts()
    {
        return $this->hasMany(Post::class, 'fk_u_id');
    }

    public function answers()
    {
        return $this->hasMany(PostAnswer::class, 'fk_u_id');
    }

    public function votes()
    {
        return $this->hasMany(PostAnswerVote::class, 'fk_u_id');
    }

    public function settings()
    {
        return $this->hasOne(UserSetting::class, 'fk_u_id');
    }

    /*
     * ----------------------------- FUNCTIONS --------------------------------
     */

    /**
     * Return a closure for the eager loading query
     *
     * @return \Closure
     */
    public static function constraintEagerLoadingQuery() : Closure
    {
        return function ($query) {
            $query->select([
                'tbl_user.u_id',
                'tbl_user.u_firstName',
                'tbl_user.u_lastName'
            ])->withTrashed()->withoutGlobalScopes();
        };
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Set password and hash it
     *
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = Hash::make(trim($password));
    }

    /**
     * Get full name (first + last)
     *
     * @return string
     */
    public function getFullName() : string
    {
        return $this->u_firstName . ' ' . $this->u_lastName;
    }

    /**
     * Get the filename for the users profile picture
     *
     * @return string
     */
    public function generateProfilePictureFileName() : string
    {
        return $this->getKey() . ':'  . sha1(time() . time());
    }

    /**
     * Get the profile pic path
     *
     * @return string
     */
    public function getProfilePictureFileName()
    {
        return $this->u_profilePicture;
    }

    /**
     * Check if user is superuser
     *
     * @return bool
     */
    public function isSuperuser() : bool
    {
        return $this->u_superuser;
    }

    /**
     * Check if user is active
     *
     * @return bool
     */
    public function isActive() : bool
    {
        return $this->u_active;
    }

    public function getSettings()
    {
        $settings = $this->settings;

        if (! $settings) {
            $settings = UserSetting::create([
                'fk_u_id' => $this->getKey()
            ]);

            $this->update(['fk_us_id' => $settings->getKey()]);
        }

        return $settings;
    }

    /**
     * Check if user already voted for a specific post answer
     *
     * @param int|null $pa_id   Post answer id
     * @return bool
     */
    public function alreadyVoted(int $pa_id = null) : bool
    {
        if (! $pa_id) {
            return false;
        }

        $u_id = $this->getKey();
        $cache_key = str_replace(['###UID###', '###PAID###'], [$u_id, $pa_id], self::CACHE_KEY_ANSWER_VOTED);

        if (Cache::has($cache_key)) {
            $voted = (bool) Cache::get($cache_key);
        } else {
            $vote = PostAnswerVote::where('fk_u_id', $u_id)->where('fk_pa_id', $pa_id)->first();
            $voted = $vote ? true : false;

            Cache::forever($cache_key, $voted);
        }

        return $voted;
    }
}
