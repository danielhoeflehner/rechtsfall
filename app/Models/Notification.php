<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends BaseModel
{
    use SoftDeletes;

    protected $table = 'tbl_notification';
    protected $primaryKey = 'n_id';

    protected $casts = [
        'n_description' => 'array'
    ];

    protected $guarded = [];

    /*
     * --------------------------- CONSTANTS ----------------------------------
     */


    /*
     * ------------------------------ BOOT ------------------------------------
     */

    protected static function boot()
    {
        parent::boot();
    }

    /*
     * ------------------------------ SCOPES ----------------------------------
     */

    public function scopeForAuthenticatedUser($query, int $id = null)
    {
        if (! $id) {
            $id = auth()->id();
        }

        return $query->where('fk_u_id', $id)
            ->latest()
            ->limit(7)
            ->get([
                'n_id',
                'n_title',
                'n_description',
                self::CREATED_AT
            ]);
    }

    /*
     * ---------------------------- RELATIONS ---------------------------------
     */

    public function user()
    {
        return $this->belongsTo(User::class, 'fk_u_id');
    }

    /*
     * ----------------------------- FUNCTIONS --------------------------------
     */

    public function getFormattedCreatedAttribute()
    {
        return $this->created_at->diffForHumans();
    }
}
