<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Auth;
use Cache;

class PostAnswerVote extends BaseModel
{
    use SoftDeletes;

    protected $table = 'tbl_postAnswerVote';
    protected $primaryKey = 'pav_id';

    protected $casts = [
        'pav_vote' => 'boolean'
    ];

    protected $guarded = [];

    /*
     * --------------------------------------------------------------------
     */


    protected static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->fk_u_id = Auth::id();

            Cache::forever(str_replace(['###UID###', '###PAID###'], [$model->fk_u_id, $model->fk_pa_id], User::CACHE_KEY_ANSWER_VOTED), 1);
        });
    }

    /*
     * --------------------------------------------------------------------
     */


    public function answer()
    {
        return $this->belongsTo(PostAnswer::class, 'fk_pa_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'fk_u_id');
    }


    /*
     * --------------------------------------------------------------------
     */
}
