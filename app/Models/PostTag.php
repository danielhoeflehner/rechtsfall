<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;


class PostTag extends BaseModel
{
    protected $table = 'tbl_postTag';
    protected $primaryKey = 'pt_id';

    use SoftDeletes;

    /*
     * ------------------------------ BOOT ------------------------------------
     */


    protected static function boot()
    {
        parent::boot();
    }


    /*
     * ------------------------------ SCOPES ----------------------------------
     */



    /*
     * ---------------------------- RELATIONS ---------------------------------
     */

    public function post()
    {
        return $this->belongsTo(Post::class, 'fk_p_id', 'p_id');
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class, 'fk_t_id', 't_id');
    }

    /*
     * ----------------------------- FUNCTIONS --------------------------------
     */
}
