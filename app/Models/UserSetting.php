<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSetting extends BaseModel
{
    protected $table = 'tbl_userSetting';
    protected $primaryKey = 'us_id';

    protected $casts = [
        'us_emailPostGetsAnswer' => 'boolean',
        'us_emailPrivateMessageReceived' => 'boolean',
        'us_notificationPostGetsAnswer' => 'boolean',
        'us_notificationAnswerGetsVote' => 'boolean',
        'us_notificationPrivateMessageReceived' => 'boolean',
    ];

    use SoftDeletes;

    /*
     * --------------------------- CONSTANTS ----------------------------------
     */

    /*
     * ------------------------------ BOOT ------------------------------------
     */

    protected static function boot()
    {
        parent::boot();
    }

    /*
     * ---------------------------- RELATIONS ---------------------------------
     */

    public function user()
    {
        return $this->hasOne(User::class, 'fk_us_id');
    }

    /*
     * ----------------------------- FUNCTIONS --------------------------------
     */
}
