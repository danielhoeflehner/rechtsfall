<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Settings extends BaseModel
{
    protected $table = 'tbl_settings';
    protected $primaryKey = 's_id';

    protected $casts = [
        's_settings' => 'array'
    ];

    const IDENTIFIER_EMAIL = 'email';
    const IDENTIFIER_EMAIL_POST_GETS_ANSWER = 'email_post_gets_answer';
    const IDENTIFIER_EMAIL_ANSWER_GETS_STATUS = 'email_answer_gets_status';
    const IDENTIFIER_EMAIL_PRIVATE_MESSAGE_RECEIVED = 'email_private_message_received';

    const IDENTIFIER_NOTIFICATION = 'notification';
    const IDENTIFIER_NOTIFICATION_POST_GETS_ANSWER = 'notification_post_gets_answer';
    const IDENTIFIER_NOTIFICATION_ANSWER_GETS_VOTE = 'notification_answer_gets_vote';
    const IDENTIFIER_NOTIFICATION_ANSWER_GETS_STATUS = 'notification_answer_gets_status';
    const IDENTIFIER_NOTIFICATION_PRIVATE_MESSAGE_RECEIVED = 'notification_private_message_received';

    use SoftDeletes;


    /*
     * --------------------------------------------------------------------
     */


    public function scopeEmail(Builder $query) : Settings
    {
        return $query->where('s_identifier', self::IDENTIFIER_EMAIL)->first();
    }

    public function scopeEmailIdentifiers(Builder $query) : array
    {
        $identifiers = $query->where('s_identifier', self::IDENTIFIER_EMAIL)
            ->select([DB::raw("JSON_EXTRACT(s_settings, '$[*].identifier') AS identifiers")])
            ->value('identifiers');

        return $identifiers ? json_decode($identifiers) : [];
    }

    public function scopeNotification(Builder $query) : Settings
    {
        return $query->where('s_identifier', self::IDENTIFIER_NOTIFICATION)->first();
    }

    public function scopeNotificationIdentifiers(Builder $query) : array
    {
        $identifiers = $query->where('s_identifier', self::IDENTIFIER_NOTIFICATION)
            ->select([DB::raw("JSON_EXTRACT(s_settings, '$[*].identifier') AS identifiers")])
            ->value('identifiers');

        return $identifiers ? json_decode($identifiers) : [];
    }


    /*
     * --------------------------------------------------------------------
     */


    public function getTitle() : string
    {
        return $this->s_title;
    }

    public function getDescription() : string
    {
        return $this->s_desc;
    }

    public function getSettings() : array
    {
        return $this->s_settings;
    }
}
