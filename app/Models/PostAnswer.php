<?php

declare(strict_types=1);

namespace App\Models;

use App\Scopes\ActivePostAnswerScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

use Auth;
use Cache;

class PostAnswer extends BaseModel
{
    use SoftDeletes;

    protected $table = 'tbl_postAnswer';
    protected $primaryKey = 'pa_id';

    protected $casts = [
        'pa_active'      => 'boolean',
        'pa_confirmed'   => 'boolean',
        'pa_declined'    => 'boolean'
    ];

    protected $guarded = [];

    /*
     * --------------------------- CONSTANTS ----------------------------------
     */

    /*
     * ------------------------------ BOOT ------------------------------------
     */


    protected static function boot()
    {
        parent::boot();

        // Add the global scope => just answers which are active
        static::addGlobalScope(new ActivePostAnswerScope);

        static::creating(function ($model) {
            $model->fk_u_id = Auth::id();
        });
    }


    /*
     * ------------------------------ SCOPES ----------------------------------
     */


    public function scopeJoinUser($query)
    {
        $query->join('tbl_user', 'tbl_postAnswer.fk_u_id', '=', 'tbl_user.u_id');
    }

    public function scopeJoinVotes($query)
    {
        $query->join('tbl_postAnswerVote', 'tbl_postAnswer.pa_id', '=', 'tbl_postAnswerVote.fk_pa_id');
    }


    /*
     * ---------------------------- RELATIONS ---------------------------------
     */


    public function votes()
    {
        return $this->hasMany(PostAnswerVote::class, 'fk_pa_id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'fk_p_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'fk_u_id');//->withTrashed()->withoutGlobalScope();
    }

    /*
     * ----------------------- ACCESSORS & MUTATORS ---------------------------
     */

    public function getFormattedCreatedAtAttribute() : string
    {
        return $this->created_at->diffForHumans();
    }


    /*
     * ----------------------------- FUNCTIONS --------------------------------
     */

    public function getBody() : string
    {
        return $this->pa_body;
    }

    public function isActive() : bool
    {
        return $this->pa_active;
    }

    public function isDeclined() : bool
    {
        return $this->pa_declined;
    }

    public function getDeclinedAt()
    {
        return $this->pa_declinedAt;
    }

    public function isConfirmed() : bool
    {
        return $this->pa_confirmed;
    }

    public function getConfirmedAt()
    {
        return $this->pa_confirmedAt;
    }

    public function getUpVotesCount() : int
    {
        return (int) $this->pa_votesUp;
    }

    public function getDownVotesCount() : int
    {
        return (int) $this->pa_votesDown;
    }
}
