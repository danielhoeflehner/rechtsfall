<?php

declare(strict_types=1);

namespace App\Models;

use App\Scopes\ActivePostScope;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends BaseModel
{
    use SoftDeletes;

    protected $table = 'tbl_post';
    protected $primaryKey = 'p_id';

    protected $casts = [
        'p_active'      => 'boolean',
        'p_answerCount' => 'integer',
        'p_confirmed'   => 'boolean',
        'p_tags'        => 'array',
        'p_stats'       => 'array'
    ];

    protected $guarded = [];

    /*
     * ------------------------------ BOOT ------------------------------------
     */

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActivePostScope);
    }

    /*
     * ------------------------------ SCOPES ----------------------------------
     */

    /*
     * ---------------------------- RELATIONS ---------------------------------
     */

    public function user()
    {
        return $this->belongsTo(User::class, 'fk_u_id');
    }

    public function answers()
    {
        return $this->hasMany(PostAnswer::class, 'fk_p_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'tbl_postTag', 'fk_p_id', 'fk_t_id');
    }

    /*
     * ----------------------- ACCESSORS & MUTATORS ---------------------------
     */

    public function getFormattedCreatedAtAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function getShortenedBodyAttribute()
    {
        return trim(Str::limit(strip_tags(str_replace(['<br>', '<p>'], ' ', $this->p_body)), 100));
    }

    public function getPConfirmedAtAttribute()
    {
        return $this->p_confirmedAt->diffForHumans();
    }

    /*
     * ----------------------------- FUNCTIONS --------------------------------
     */

    public function getTitle() : string
    {
        return $this->p_title;
    }

    public function getBody() : string
    {
        return $this->p_body;
    }

    public function getTags() : array
    {
        return $this->p_tags;
    }

    public function getStats() : array
    {
        return $this->p_stats;
    }

    public function getAnswerCount() : int
    {
        return $this->p_answerCount;
    }

    public function isActive() : bool
    {
        return $this->p_active;
    }

    public function isConfirmed() : bool
    {
        return $this->p_confirmed;
    }

    public function getFormattedAnswerCount() : string
    {
        switch ($this->p_answerCount) {
            case 1:
                return $this->p_answerCount . ' Antwort';
                break;

            default:
                return $this->p_answerCount . ' Antworten';
        }
    }

    public function attachTags(array $requestTags)
    {
        $tags = [];

        foreach ($requestTags as $tag_array) {
            $key = key($tag_array);
            $value = $tag_array[key($tag_array)];

            $tag = Tag::createOrUpdateTag($key, $value);

            if ($tag) {
                $this->tags()->attach($tag->getKey());
                array_push($tags, ['id' => $tag->getKey(), 'value' => $tag->t_name]);
            }
        }

        $this->p_tags = $tags;
    }
}
