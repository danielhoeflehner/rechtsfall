<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'post_title'        => 'bail|required|max:255',
            'post_content_html' => 'bail|required|min:10',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() : array
    {
        return [
            'post_title.required'           => 'Bitte gib einen Title an.',
            'post_title.max'                => 'Der Titel ist zu lang.',
            'post_content_html.required'    => 'Bitte füll das Formular vollständig aus.',
            'post_content_html.min'         => 'Bitte beschreib den Fall genauer.'
        ];
    }
}
