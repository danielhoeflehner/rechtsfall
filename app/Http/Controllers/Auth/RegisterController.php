<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstName' => 'required|max:255',
            'lastName'  => 'required|max:255',
            'email'     => 'required|email|max:255|unique:tbl_user,email',
            'password'  => 'required|min:6',
        ], [
            'firstName.required'    => 'Gib bitte deinen Vornamen an.',
            'firstName.max'         => 'Dein Vorname ist nicht normal.',
            'lastName.required'     => 'Gib bitte deinen Nachnamen an.',
            'lastName.max'          => 'Dein Nachname ist nicht normal.',
            'email.required'        => 'Gib bitte deine Email Adresse an.',
            'email.email'           => 'Gib bitte eine gültige Email Addresse an.',
            'email.max'             => 'Deine Email Adresse ist zu lang',
            'email.unique'          => 'Nope.',
            'password.required'     => 'Gib bitte ein Passwort an.',
            'password.min'          => 'Dein Passwort muss mindestens 6 Zeichen lang sein.'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'u_firstName'   => $data['firstName'],
            'u_lastName'    => $data['lastName'],
            'email'         => $data['email'],
            'password'      => bcrypt($data['password']),
            'u_active'      => true
        ]);
    }
}
