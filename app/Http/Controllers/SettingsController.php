<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Navigation;
use App\Models\Settings;
use Illuminate\Http\Request;

use App\Http\Requests;

class SettingsController extends Controller
{
    public function settings()
    {
        $email_settings = Settings::Email();

        return view('admin.settings', [
            'email_settings' => $email_settings,
            'nav' => Navigation::NAV_ADMIN,
            'subnav' => Navigation::NAV_ADMIN_SETTINGS
        ]);
    }
}
