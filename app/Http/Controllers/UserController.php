<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Navigation;
use App\Models\Settings;
use App\Models\User;
use App\Models\UserSetting;

use Illuminate\Http\Request;

use DB;
use File;
use Hash;
use Storage;

class UserController extends Controller
{
    /**
     * Show the users profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view('user.profile.private', [
            'user' => auth()->user(),
            'nav' => Navigation::NAV_PROFILE,
            'subnav' => ''
        ]);
    }

    public function profilePic(Request $request)
    {
        $user = $request->user();

        if (! Storage::disk('profilePics')->exists($user->getProfilePictureFileName())) {
            return response()->file(storage_path('app/profilePics/') . User::DEFAULT_PROFILE_PICTURE);
        }

        return response()->file(storage_path('app/profilePics/') . $user->getProfilePictureFileName());
    }


    /**
     * Show the users settings.
     * - settings
     * - general profile data
     * - password
     *
     * @param string $tab
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function settings($tab = 'general')
    {
        return view('user.settings', [
            'user'     => auth()->user(),
            'settings' => auth()->user()->getSettings(),
            'tab'      => $tab,
            'nav'      => Navigation::NAV_SETTINGS,
            'subnav'   => ''
        ]);
    }


    public function saveGeneral(Request $request)
    {
        $request->user()->settings()->update([
            'us_emailPostGetsAnswer'                => isset($request->emailPostGetsAnswer) ? true : false,
            'us_emailPrivateMessageReceived'        => isset($request->emailPrivateMessageReceived) ? true : false,
            'us_notificationPostGetsAnswer'         => isset($request->notificationPostGetsAnswer) ? true : false,
            'us_notificationAnswerGetsVote'         => isset($request->notificationAnswerGetsVote) ? true : false,
            'us_notificationPrivateMessageReceived' => isset($request->notificationPrivateMessageReceived) ? true : false,
        ]);

        return redirect()->route('user.settings.get', ['tab' => 'general'])
            ->with('success', 'Deine Daten wurden aktualisiert.');
    }


    /**
     * Save the users general profile data.
     *
     * @param Request $request
     * @param string $tab
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveProfile(Request $request, $tab = 'profile')
    {
        $validator = validator($request->all(), [
            'profilePic' => 'file|image|max:1024',
            'firstName'  => 'bail|required|max:100',
            'lastName'   => 'bail|required|max:100',
            'email'      => 'bail|required|email',
            'city'       => 'max:100',
            'job'        => 'max:100',
            'university' => 'max:100'
        ], [
            'profilePic.file'    => 'Datei konnte nicht hochgeladen werden.',
            'profilePic.image'   => 'Die Datei muss ein Bild (jpg oder png) sein.',
            'profilePic.max'     => 'Die Datei ist zu groß (max 1MB).',
            'firstName.required' => 'Bitte gib deinen Vornamen an.',
            'firstName.max'      => 'Dein Vorname ist zu lang.',
            'lastName.required'  => 'Bitte gib deinen Nachnamen an.',
            'lastName.max'       => 'Dein Nachname ist zu lang.',
            'email.required'     => 'Bitte gib eine Email Adresse an.',
            'email.email'        => 'Bitte gib eine gültige Email Adresse an.',
            'city.max'           => 'Stadt ist zu lang.',
            'job.max'            => 'Beruf ist zu lang.',
            'university.max'     => 'Universität ist zu lang.'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('user.settings.profile.get')
                ->withErrors($validator)
                ->withInput();
        }

        DB::beginTransaction();

        $user = $request->user();
        $user->u_firstName  = $request->firstName;
        $user->u_lastName   = $request->lastName;
        $user->email        = $request->email;
        $user->u_city       = $request->city;
        $user->u_job        = $request->job;
        $user->u_university = $request->university;

        if ($request->profilePic) {
            $file = $request->file('profilePic');
            $fileName = $user->getProfilePicturePath();

            if (!empty($fileName) && Storage::disk('profilePics')->exists($fileName)) {
                Storage::disk('profilePics')->delete($fileName);
            }

            $fileName = $user->generateProfilePictureFileName() . '.' . $file->getClientOriginalExtension();

            Storage::disk('profilePics')->put(
                $fileName,
                File::get($file)
            );

            $user->u_profilePicture = $fileName;
        }

        $user->save();

        DB::commit();

        return redirect()->route('user.profile.get')
            ->with('success', 'Deine Daten wurden aktualisiert.');
    }


    /**
     * Save the users new password.
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function savePassword(Request $request)
    {
        $validator = validator($request->all(), [
            'oldPassword'           => 'required',
            'password'              => 'bail|required|min:6|max:100',
            'password_confirmation' => 'bail|required|same:password'
        ], [
            'oldPassword.required'              => 'Bitte gib dein altes Passwort an.',
            'password.required'                 => 'Bitte gib ein neues Passwort an (mindestens 6 Zeichen).',
            'password.min'                      => 'Das Passwort ist zu kurz (mindestens 6 Zeichen).',
            'password.max'                      => 'Das Passwort ist zu lang (maximal 100 Zeichen).',
            'password_confirmation.required'    => 'Bitte bestätige das Passwort.',
            'password_confirmation.same'        => 'Passwörter müssen gleich sein.'
        ]);

        if ($validator->fails()) {
            return redirect()->route('user.settings.get', ['tab' => 'password'])
                ->withErrors($validator);
        }

        $user = $request->user();

        // Check if the old password matches the input
        if (! Hash::check($request->oldPassword, $user->getPassword())) {
            return redirect()->route('user.settings.get', ['tab' => 'password'])
                ->with('danger', 'Überprüfe ob du dein aktuelles Passwort richtig eingegeben hast.');
        }

        $user->setPassword($request->password);
        $user->save();

        return redirect()->route('user.settings.get', ['tab' => 'password'])
            ->with('success', 'Dein Passwort wurde aktualisiert.');
    }
}