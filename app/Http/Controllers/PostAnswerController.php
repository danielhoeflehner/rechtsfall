<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Mail\PostGotAnswered;
use App\Models\Notification;
use App\Models\Post;
use App\Models\PostAnswer;
use App\Models\PostAnswerVote;

use App\Models\Settings;
use App\Services\MyMail;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Http\Request;

use Auth;
use Response;
use DB;

class PostAnswerController extends Controller
{
    /**
     * Store a new answer to a post.
     *
     * @param Request   $request
     * @param Post      $post
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, Post $post)
    {
        DB::beginTransaction();

        PostAnswer::create([
            'pa_body' => $request->answer_content_html,
            'pa_active' => true,
            'fk_p_id' => $post->getKey()
        ]);

        $post->update([
            'p_answerCount' => $post->getAnswerCount() + 1,
            'p_stats' => DB::raw("p_stats->'users' || '[" . auth()->id() . "]'::jsonb"),
        ]);

        DB::commit();

        // If the creator of the post wants to be informed, that a new answer was posted,
        // send email notification
        /*$postCreator = $post->user;
        $email = $postCreator->u_email;
        $name = $postCreator->getFullName();

        if ($postCreator->getKey() != auth()->id()) {

            $settings = $postCreator->settings;

            if ($settings->us_emailPostGetsAnswer == true) {
                try {
                    $mailable = new PostGotAnswered($postCreator, $post);

                    $mail = new MyMail($mailable, $email, $name);
                    $mail->setSubject('rechtsfall.at - Antwort auf deinen Beitrag');
                    $mail->send();
                } catch (\Exception $e) {
                    Bugsnag::notifyException($e, function ($report) use ($post, $email, $name) {
                        $report->setSeverity('info');
                        $report->setMetaData([
                            Settings::IDENTIFIER_EMAIL_POST_GETS_ANSWER => [
                                'post_id' => $post->getKey(),
                                'type' => 'email',
                                'to' => $email,
                                'name' => $name
                            ]
                        ]);
                    });
                }
            }

            if ($settings->us_notificationPostGetsAnswer == true) {
                // TODO: broadcast notification
            }

            $notification = new Notification();
            $notification->n_title = 'Antwort auf deinen Beitrag';
            $notification->n_description = [
                'link' => '/post/' . $post->getKey() . '/show',
                'text' => '<span class="name">' . $user->u_firstName . '</span> hat auf deinen Beitrag geantwortet.'
            ];
            $notification->n_notifyAt = DB::raw('NOW()');
            $notification->fk_u_id = $postCreator->getKey();
            $notification->save();
        }*/

        session()->flash('success', 'Deine Antwort wurde erfolgreich gespeichert.');
        return redirect()->route('post.show.get', ['post' => $post->getKey()]);
    }

    /**
     * AJAX
     * Store an up/down vote from an user on an answer.
     *
     * @param Request   $request
     * @param PostAnswer $answer
     * @return mixed
     */
    public function vote(Request $request, PostAnswer $answer)
    {
        $user = $request->user();
        $userAlreadyVoted = $user->alreadyVoted($answer->getKey());

        if (! $userAlreadyVoted) {
            $answerCreator = $answer->user;

            switch($request->type) {
                case 'down':
                    PostAnswerVote::create([
                        'pav_vote' => false,
                        'fk_pa_id' => $answer->getKey()
                    ]);

                    $answer->increment('pa_votesDown');
                    $answer->save();

                    Notification::create([
                        'n_title' => 'Antwort downgevoted',
                        'n_description' => [
                            'link' => route('post.show.get', ['post' => $answer->fk_p_id]),
                            'text' => '<span class="name">' . $user->u_firstName . '</span> hat deine Antwort negativ bewertet.'
                        ],
                        'n_notifyAt' => DB::raw('NOW()'),
                        'fk_u_id' => $answerCreator->getKey()
                    ]);

                    break;

                case 'up':
                    PostAnswerVote::create([
                        'pav_vote' => true,
                        'fk_pa_id' => $answer->getKey()
                    ]);

                    $answer->increment('pa_votesUp');
                    $answer->save();

                    Notification::create([
                        'n_title' => 'Antwort upgevoted',
                        'n_description' => [
                            'link' => route('post.show.get', ['post' => $answer->fk_p_id]),
                            'text' => '<span class="name">' . $user->u_firstName . '</span> hat deine Antwort positiv bewertet.'
                        ],
                        'n_notifyAt' => DB::raw('NOW()'),
                        'fk_u_id' => $answerCreator->getKey()
                    ]);

                    break;
            }
        } else {
            return [
                'error' => true,
                'msg' => 'Du hast für diese Antwort bereits abgestimmt.'
            ];
        }

        return [
            'error' => false
        ];
    }
}
