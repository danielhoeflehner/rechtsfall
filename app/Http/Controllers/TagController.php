<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Tag;

use DB;

class TagController extends Controller
{
    /**
     * AJAX
     * Find first 10 tags which begin with the searched input
     *
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        $search = Tag::formatIdentifier($request->search) . '%';

        $tags = Tag::where('t_identifier', 'LIKE', $search)
            ->select([
                DB::raw('t_id AS id'),
                DB::raw('t_name AS value')
            ])
            ->limit(10)
            ->get()
            ->toArray();

        return [
            'error' => false,
            'suggs' => $tags
        ];
    }
}
