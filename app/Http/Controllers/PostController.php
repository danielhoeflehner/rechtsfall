<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\StorePost;
use App\Models\Navigation;
use App\Models\Notification;
use App\Models\Post;
use App\Models\User;

use Illuminate\Http\Request;

use DB;

/**
 * Class PostController
 * @package App\Http\Controllers
 */
class PostController extends Controller
{
    /**
     * Display a listing of all posts.
     *
     * @return View
     */
    public function index()
    {
        $posts = Post::with(['user' => User::constraintEagerLoadingQuery()])
            ->latest()
            ->paginate(12);

        return view('post.posts', [
            'posts'  => $posts,
            'nav'    => Navigation::NAV_POSTS,
            'subnav' => ''
        ]);
    }

    /**
     * Show posts by selected tags.
     *
     * @param int|null      $t_id
     * @return \Illuminate\Http\RedirectResponse|View
     */
    public function showByTag(int $t_id = null)
    {
        if(! is_numeric($t_id)) {
            return back()->with('error', 'Kann keine Einträge zu diesem Tag finden.');
        }

        $posts = Post::with(['user' => User::constraintEagerLoadingQuery()])
            ->join('tbl_postTag', 'tbl_post.p_id', '=', 'tbl_postTag.fk_p_id')
            ->where('tbl_postTag.fk_t_id', $t_id)
            ->latest('tbl_post.created_at')
            ->paginate(3, ['tbl_post.*']);

        return view('post.posts', [
            'posts'  => $posts,
            'nav'    => Navigation::NAV_POSTS,
            'subnav' => ''
        ]);
    }

    /**
     * Show the form for creating a new post.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create', [
            'nav'    => Navigation::NAV_POST_NEW,
            'subnav' => ''
        ]);
    }

    /**
     * Store a newly created post in storage.
     *
     * @param  StorePost  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        DB::beginTransaction();

        $post = Post::make([
            'p_active'  => true,
            'p_title'   => $request->post_title,
            'p_body'    => $request->post_content_html,
            'p_tags'    => [],
            'p_stats'   => [],
            'fk_u_id'   => $request->user()->getKey()
        ]);

        $post->attachTags($request->post_tags);
        $post->save();

        DB::commit();

        return redirect()->route('post.get')
            ->with('success', 'Beitrag wurde erfolgreich erstellt.');
    }

    /**
     * Display the specified post.
     *
     * @param  Post $post
     * @return View
     */
    public function show(Post $post)
    {
        $post->load(['answers' => function ($query) {
            $query->with(['user' => User::constraintEagerLoadingQuery()])
                ->select([
                    DB::raw('("tbl_postAnswer"."pa_votesUp" - "tbl_postAnswer"."pa_votesDown") as ranking'),
                    'tbl_postAnswer.*',
                ])->orderBy('ranking', 'desc');
        }]);

        return view('post.post', [
            'post'      => $post,
            'answers'   => $post->answers,
            'nav'       => Navigation::NAV_POSTS,
            'subnav'    => ''
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Post $post
     * @return View
     */
    public function edit(Post $post)
    {
        return view('post.postForm', [
            'post'   => $post,
            'nav'    => Navigation::NAV_POST,
            'subnav' => ''
        ]);
    }

    /**
     * Update the specified post in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        DB::beginTransaction();

        $post->p_active = true;
        $post->p_title  = $request->post_title;
        $post->p_body   = $request->post_content_html;
        $post->fk_u_id  = auth()->id();
        $post->save();

        DB::commit();

        return redirect()->route('post.show.get', ['post' => $post->getKey()])
            ->with('success', 'Beitrag wurde upgedated.');
    }

    /**
     * AJAX
     * Mark a post as solved.
     *
     * @param Request $request
     * @param Post $post
     * @return mixed
     */
    public function solved(Request $request, Post $post)
    {
        DB::beginTransaction();

        $post->update([
            'p_confirmed'   => $request->type,
            'p_confirmedAt' => DB::raw('now()')
        ]);

        DB::commit();

        foreach($post->p_stats as $user_id) {
            Notification::create([
                'n_title' => 'Beitrag gelöst',
                'n_description' => [
                    'link' => route('post.show.get', ['post' => $post->getKey()]),
                    'text' => '<span class="name">' . auth()->user()->u_firstName . '</span> hat einen Beitrag, auf den du eine Antwort geschrieben hast, als gelöst markiert.'
                ],
                'n_notifyAt' => DB::raw('NOW()'),
                'fk_u_id' => $user_id
            ]);
        }

        return [
            'error' => false,
            'msg' => 'Erfolgreich gespeichert.'
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
