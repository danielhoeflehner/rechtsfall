<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\Post;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with(['user' => User::constraintEagerLoadingQuery()])
            ->latest(Post::CREATED_AT)
            ->limit(10)
            ->get();

        $notifications = Notification::forAuthenticatedUser();

        return view('home', [
            'posts' => $posts,
            'notifications' => $notifications,
            'nav' => 'home',
            'subnav' => ''
        ]);
    }
}
