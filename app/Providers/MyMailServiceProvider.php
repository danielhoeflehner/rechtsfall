<?php

namespace App\Providers;

use App\Services\MyMail;
use Illuminate\Support\ServiceProvider;

class MyMailServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MyMail::class, function () {
            return new MyMail();
        });
    }

    public function provides()
    {
        return [MyMail::class];
    }
}
