<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('time', function ($expression) {
            return "<?php echo with(new \\DateTime({$expression}))->format('H:i'); ?>";
        });

        Blade::directive('timeexact', function ($expression) {
            return "<?php echo with(new \\DateTime({$expression}))->format('H:i:s'); ?>";
        });

        Blade::directive('date', function ($expression) {
            return "<?php echo with(new \\DateTime({$expression}))->format('d.m.Y'); ?>";
        });

        Blade::directive('datetime', function ($expression) {
            return "<?php echo with(new \\DateTime({$expression}))->format('d.m.Y H:i'); ?>";
        });

        Blade::directive('datetimeexact', function ($expression) {
            return "<?php echo with(new \\DateTime({$expression}))->format('d.m.Y H:i:s'); ?>";
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
