<?php

namespace App\Services;

use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

class MyMail
{
    private $from        = 'hello@rechtsfall.at';
    private $fromName    = 'rechtsfall';
    private $sender      = 'hello@rechtsfall.at';
    private $senderName  = 'rechtsfall';
    private $to;
    private $toName;
    private $cc          = null;
    private $bcc         = null;
    private $replyTo     = 'office@rechtsfall.at';
    private $replyToName = 'rechtsfall';
    private $view        = 'emails.default';
    private $data;
    private $subject;


    function __construct(Mailable $mailable, string $email, string $name)
    {
        $this->mailable = $mailable;

        $this->mailable->from($this->from, $this->fromName);

        $this->to = $email;
        $this->toName = $name;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject(string $subject)
    {
        $this->subject = $subject;
        $this->mailable->subject($this->subject);
    }

    /**
     * @param mixed $cc
     */
    public function setCc($cc)
    {
        $this->cc = $cc;
    }

    /**
     * @param mixed $bcc
     */
    public function setBcc($bcc)
    {
        $this->bcc = $bcc;
    }

    /**
     * @param string $replyTo
     */
    public function setReplyTo(string $replyTo)
    {
        $this->replyTo = $replyTo;
    }

    /**
     * @param string $replyToName
     */
    public function setReplyToName(string $replyToName)
    {
        $this->replyToName = $replyToName;
    }

    /**
     * @param $view
     */
    public function setView(string $view)
    {
        $this->view = $view;
    }

    /**
     * @param mixed $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    public function send()
    {
        if (app()->environment('production')) {
            $send = Mail::to($this->to, $this->toName);

            if ($this->cc) {
                $send = $send->cc($this->cc);
            }

            if ($this->bcc) {
                $send = $send->bcc($this->bcc);
            }
        } else {
            $send = Mail::to(env('DEV_TO_MAIL'));
        }

        $send->queue($this->mailable);
    }
}