<?php

namespace App\Listeners;

use App\Events\Login;
use App\Models\LoginLog;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;
use Agent;

class UserEventSubscriber
{
    /**
     * Handle user login events.
     */
    public function onUserLogin($event)
    {
        try {

            $user = $event->user;

            DB::beginTransaction();

            $loginLog = new LoginLog();
            $loginLog->ll_userAgent         = Agent::getUserAgent();
            $loginLog->ll_device            = Agent::device();
            $loginLog->ll_platform          = Agent::platform();
            $loginLog->ll_platformVersion   = Agent::version($loginLog->ll_platform);
            $loginLog->ll_browser           = Agent::browser();
            $loginLog->ll_browserVersion    = Agent::version($loginLog->ll_browser);
            $loginLog->ll_desktop           = Agent::isDesktop();
            $loginLog->ll_mobile            = Agent::isMobile();
            $loginLog->ll_tablet            = Agent::isTablet();
            $loginLog->ll_phone             = Agent::isPhone();
            $loginLog->ll_ip                = $_SERVER['REMOTE_ADDR'] ?? 'unknown';
            $loginLog->fk_u_id              = $user->getKey();
            $loginLog->save();

            $user->u_lastLoginAt            = DB::raw('NOW()');
            $user->save();

            DB::commit();

        } catch (\Exception $e) {
            dd($e->getMessage());
        }

    }

    /**
     * Handle user logout events.
     */
    public function onUserLogout($event)
    {

    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Login',
            'App\Listeners\UserEventSubscriber@onUserLogin'
        );

        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'App\Listeners\UserEventSubscriber@onUserLogout'
        );
    }
}
