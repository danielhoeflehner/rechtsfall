<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;


class MyMail extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'MyMail';
    }
}