<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFkUsIdToTblUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_user', function (Blueprint $table) {
            $table->integer('fk_us_id')->unsigned()->nullable()->after('u_lastLoginAt');
            $table->foreign('fk_us_id')->references('us_id')->on('tbl_userSetting')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_user', function (Blueprint $table) {
            $table->dropForeign(['fk_us_id']);
            $table->dropColumn('fk_us_id');
        });
    }
}
