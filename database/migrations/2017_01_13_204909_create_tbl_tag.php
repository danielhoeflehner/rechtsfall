<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblTag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_tag', function (Blueprint $table) {
            $table->increments('t_id');
            $table->string('t_identifier');
            $table->string('t_name');
            $table->integer('t_count')->default(0);
            $table->integer('fk_u_id')->unsigned()->nullable();

            $table->foreign('fk_u_id')->references('u_id')->on('tbl_user')->onUpdate('cascade')->onDelete('set null');

            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_tag');
    }
}
