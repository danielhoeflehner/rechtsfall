<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_user', function (Blueprint $table) {
            $table->increments('u_id');
            $table->string('u_firstName');
            $table->string('u_lastName');
            $table->string('u_city')->default('');
            $table->string('u_job')->default('');
            $table->string('u_university')->default('');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('u_profilePicture')->nullable();
            $table->boolean('u_superuser')->default(0);
            $table->boolean('u_active')->default(1);
            $table->timestamp('u_lastLoginAt')->nullable();
            $table->rememberToken();
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_user');
    }
}
