<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_notification', function (Blueprint $table) {
            $table->increments('n_id');
            $table->string('n_title');
            $table->json('n_description');
            $table->timestamp('n_notifyAt')->nullable();
            $table->boolean('n_readAt')->nullable();
            $table->integer('fk_u_id')->unsigned()->nullable();

            $table->foreign('fk_u_id')->references('u_id')->on('tbl_user')->onUpdate('cascade')->onDelete('set null');

            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_notification');
    }
}
