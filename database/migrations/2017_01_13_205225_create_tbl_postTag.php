<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPostTag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_postTag', function (Blueprint $table) {
            $table->increments('pt_id');
            $table->integer('fk_p_id')->unsigned()->nullable();
            $table->integer('fk_t_id')->unsigned()->nullable();

            $table->foreign('fk_p_id')->references('p_id')->on('tbl_post')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('fk_t_id')->references('t_id')->on('tbl_tag')->onUpdate('cascade')->onDelete('set null');

            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_postTag');
    }
}
