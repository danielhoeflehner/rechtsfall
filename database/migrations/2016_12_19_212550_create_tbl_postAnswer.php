<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPostAnswer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_postAnswer', function (Blueprint $table) {
            $table->increments('pa_id');
            $table->text('pa_body');
            $table->boolean('pa_active')->default(1);
            $table->boolean('pa_confirmed')->default(0);
            $table->timestamp('pa_confirmedAt')->nullable();
            $table->boolean('pa_declined')->default(0);
            $table->timestamp('pa_declinedAt')->nullable();
            $table->integer('pa_votesUp')->unsigned()->default(0);
            $table->integer('pa_votesDown')->unsigned()->default(0);

            $table->integer('fk_p_id')->unsigned()->nullable();
            $table->integer('fk_u_id')->unsigned()->nullable();
            $table->foreign('fk_p_id')->references('p_id')->on('tbl_post')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('fk_u_id')->references('u_id')->on('tbl_user')->onUpdate('cascade')->onDelete('set null');

            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_postAnswer');
    }
}
