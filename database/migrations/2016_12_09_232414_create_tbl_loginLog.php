<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblLoginLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_loginLog', function (Blueprint $table) {
            $table->increments('ll_id');
            $table->string('ll_userAgent');
            $table->string('ll_device', 50);
            $table->string('ll_platform', 50);
            $table->string('ll_platformVersion', 50);
            $table->string('ll_browser', 50);
            $table->string('ll_browserVersion', 50);
            $table->boolean('ll_desktop')->default(0);
            $table->boolean('ll_mobile')->default(0);
            $table->boolean('ll_tablet')->default(0);
            $table->boolean('ll_phone')->default(0);
            $table->ipAddress('ll_ip');

            $table->integer('fk_u_id')->unsigned()->nullable();
            $table->foreign('fk_u_id')->references('u_id')->on('tbl_user')->onUpdate('cascade')->onDelete('set null');

            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_loginLog');
    }
}
