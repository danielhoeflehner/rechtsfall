<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPostAnswerVote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_postAnswerVote', function (Blueprint $table) {
            $table->increments('pav_id');
            $table->boolean('pav_vote');
            $table->integer('fk_pa_id')->unsigned()->nullable();
            $table->integer('fk_u_id')->unsigned()->nullable();

            $table->foreign('fk_pa_id')->references('pa_id')->on('tbl_postAnswer')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('fk_u_id')->references('u_id')->on('tbl_user')->onUpdate('cascade')->onDelete('set null');

            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_postAnswerVote');
    }
}
