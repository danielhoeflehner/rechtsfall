<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_post', function (Blueprint $table) {
            $table->increments('p_id');
            $table->boolean('p_active')->default(1);
            $table->string('p_title');
            $table->text('p_body');
            $table->integer('p_answerCount')->default(0);
            $table->boolean('p_confirmed')->default(0);
            $table->timestamp('p_confirmedAt')->nullable();
            $table->jsonb('p_tags');
            $table->jsonb('p_stats');

            $table->integer('fk_u_id')->unsigned()->nullable();
            $table->foreign('fk_u_id')->references('u_id')->on('tbl_user')->onUpdate('cascade')->onDelete('set null');

            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_post');
    }
}
