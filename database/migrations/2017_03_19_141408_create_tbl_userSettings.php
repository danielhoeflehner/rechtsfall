<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblUserSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_userSetting', function (Blueprint $table) {
            $table->increments('us_id');

            $table->boolean('us_emailPostGetsAnswer')->default(0);
            $table->boolean('us_emailPrivateMessageReceived')->default(0);

            $table->boolean('us_notificationPostGetsAnswer')->default(1);
            $table->boolean('us_notificationAnswerGetsVote')->default(1);
            $table->boolean('us_notificationPrivateMessageReceived')->default(1);

            $table->integer('fk_u_id')->unsigned()->nullable();
            $table->foreign('fk_u_id')->references('u_id')->on('tbl_user')->onUpdate('cascade')->onDelete('set null');

            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_userSetting');
    }
}
