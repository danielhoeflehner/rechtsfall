<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_user')->insert([
            'u_firstName' => 'rechtsfall.at',
            'u_lastName' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('secret')
        ]);
    }
}
